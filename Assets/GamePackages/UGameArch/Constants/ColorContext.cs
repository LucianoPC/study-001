namespace UGameArch.Constants
{
    public static class ColorContext
    {
        public const string Empty      = "";
        public const string Background = "background";
        public const string Text       = "text";
    }
}