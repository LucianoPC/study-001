using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UGameArch.Providers
{
    [CreateAssetMenu(fileName = "ColorProvider.asset", menuName = "UGameArch/Color Provider", order = 0)]
    public class ColorProvider : ScriptableObject
    {
        public interface IColorCodeAdapter
        {
            string GetCode(string code, string context);
        }
        
        [SerializeField] Color defaultColor = Color.white;
        [SerializeField] ColorProvider[] colorProviders;
        [SerializeField] MapCodeToColor[] mapCodesToColors;

        Cache cache = new Cache();
        IColorCodeAdapter colorCodeAdapter = new NullObjectColorCodeAdapter();
        
        Color outColor;

        public IColorCodeAdapter ColorCodeAdapter
        {
            set => colorCodeAdapter = value;
        }

        public void Initialize()
        {
            Initialize(new NullObjectColorCodeAdapter());
        }

        public void Initialize(IColorCodeAdapter colorCodeAdapter)
        {
            cache = new Cache();
            this.colorCodeAdapter = colorCodeAdapter;
        }
        
        public Color GetColor(string code, string context)
        {
            if (cache.TryGetSprite(code, context, out outColor))
            {
                return outColor;
            }
            
            var adaptedCode = colorCodeAdapter.GetCode(code, context);

            var color = GetColor(adaptedCode);
            
            cache.AddColor(code, context, color);

            return color;
        }

        public void ClearCache()
        {
            cache.Clear();
        }
        
        Color GetColor(string code)
        {
            foreach (var colorProvider in colorProviders)
            {
                if (colorProvider.HasCode(code))
                {
                    return colorProvider.GetColor(code);
                }
            }
            
            foreach (var mapCodeToColor in mapCodesToColors)
            {
                if (mapCodeToColor.Code == code)
                {
                    return mapCodeToColor.Color;
                }
            }

            return defaultColor;
        }

        bool HasCode(string code)
        {
            return mapCodesToColors.Any(mapCodesToColor => mapCodesToColor.Code == code);
        }
        
        
        [Serializable]
        class MapCodeToColor
        {
            [SerializeField] string code;
            [SerializeField] Color color;

            public string Code => code;
            public Color Color => color;
        }

        class NullObjectColorCodeAdapter : IColorCodeAdapter
        {
            public string GetCode(string code, string context) { return code; }
        }

        class Cache
        {
            const string cacheKeyFormat = "{0}:{1}";
            
            Dictionary<string, Color> mapCodeToColor;

            public Cache()
            {
                mapCodeToColor = new Dictionary<string, Color>();
            }

            ~Cache()
            {
                Clear();
            }

            public bool TryGetSprite(string code, string context, out Color color)
            {
                var cacheKey = GetCacheKey(code, context);
                return mapCodeToColor.TryGetValue(cacheKey, out color);
            }

            public void AddColor(string code, string context, Color color)
            {
                var cacheKey = GetCacheKey(code, context);
                mapCodeToColor[cacheKey] = color;
            }

            public void Clear()
            {
                mapCodeToColor.Clear();
            }
            
            string GetCacheKey(string code, string context)
            {
                return string.Format(cacheKeyFormat, context, code);
            }
        }
    }
}