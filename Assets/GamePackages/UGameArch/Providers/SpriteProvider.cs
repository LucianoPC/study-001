using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UGameArch.Providers
{
    [CreateAssetMenu(fileName = "SpriteProvider.asset", menuName = "UGameArch/Sprite Provider", order = 0)]
    public class SpriteProvider : ScriptableObject
    {
        public interface ISpriteCodeAdapter
        {
            string GetCode(string code, string context);
        }
        
        [SerializeField] Sprite defaultSprite;
        [SerializeField] SpriteProvider[] spriteProviders;
        [SerializeField] MapCodeToSprite[] mapCodesToSprites;

        Cache cache = new Cache();
        ISpriteCodeAdapter spriteCodeAdapter = new NullObjectSpriteCodeAdapter();

        Sprite outSprite;

        public ISpriteCodeAdapter SpriteCodeAdapter
        {
            set => spriteCodeAdapter = value;
        }
        
        public void Initialize()
        {
            Initialize(new NullObjectSpriteCodeAdapter());
        }

        public void Initialize(ISpriteCodeAdapter spriteCodeAdapter)
        {
            cache = new Cache();
            this.spriteCodeAdapter = spriteCodeAdapter;
        }

        public Sprite GetSprite(string code, string context = "")
        {
            if (cache.TryGetSprite(code, context, out outSprite))
            {
                return outSprite;
            }
            
            var adaptedCode = spriteCodeAdapter.GetCode(code, context);

            var sprite = InternalGetSprite(adaptedCode);
            
            cache.AddSprite(code, context, sprite);

            return sprite;
        }

        public void ClearCache()
        {
            cache.Clear();
        }
        
        Sprite InternalGetSprite(string code)
        {
            foreach (var spriteProvider in spriteProviders)
            {
                if (spriteProvider.HasSprite(code))
                {
                    return spriteProvider.InternalGetSprite(code);
                }
            }

            foreach (var mapCodeToSprite in mapCodesToSprites)
            {
                if (mapCodeToSprite.Code == code) { return mapCodeToSprite.Sprite; }
            }

            return defaultSprite;
        }

        public bool HasSprite(string code)
        {
            return mapCodesToSprites.Any(mapCodeToSprite => mapCodeToSprite.Code == code) || spriteProviders.Any(provider => provider.HasSprite(code));
        }

        [Serializable]
        class MapCodeToSprite
        {
            [SerializeField] string code;
            [SerializeField] string prefix;
            [SerializeField] Sprite sprite;

            public string Code => $"{prefix.Trim()}{code.Trim()}";
            public Sprite Sprite => sprite;
        }

        class NullObjectSpriteCodeAdapter : ISpriteCodeAdapter
        {
            public string GetCode(string code, string context) { return code; }
        }
        
        class Cache
        {
            const string cacheKeyFormat = "{0}:{1}";
            
            Dictionary<string, Sprite> codeToSpriteMap;

            public Cache()
            {
                codeToSpriteMap = new Dictionary<string, Sprite>();
            }

            ~Cache()
            {
                Clear();
            }

            public bool TryGetSprite(string code, string context, out Sprite sprite)
            {
                var cacheKey = GetCacheKey(code, context);
                return codeToSpriteMap.TryGetValue(cacheKey, out sprite);
            }

            public void AddSprite(string code, string context, Sprite sprite)
            {
                var cacheKey = GetCacheKey(code, context);
                codeToSpriteMap[cacheKey] = sprite;
            }

            public void Clear()
            {
                codeToSpriteMap.Clear();
            }
            
            string GetCacheKey(string code, string context)
            {
                return string.Format(cacheKeyFormat, context, code);
            }
        }
    }
}