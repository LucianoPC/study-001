using UGameArch.Core;
using UGameArch.Helpers;
using UGameArch.UI;
using UnityEngine;

namespace UGameArch.Presenters
{
    public class AlertPopupPresenter
    {
        readonly AlertPopup alertPopup;
        readonly ICoroutineHelper coroutineHelper;
        
        readonly CanvasGroup canvasGroup;
        
        Coroutine showCoroutine;

        public AlertPopupPresenter(AlertPopup alertPopup, ICoroutineHelper coroutineHelper)
        {
            this.alertPopup = alertPopup;
            this.coroutineHelper = coroutineHelper;

            canvasGroup = CanvasGroupHelper.AddCanvasGroup(alertPopup.gameObject);
        }

        public void Show(string message)
        {
            alertPopup.Show(message);
            StopCoroutine();
            showCoroutine = coroutineHelper.StartCoroutine(CanvasGroupHelper.CoroutineFadeIn(canvasGroup));
        }

        public void Hide()
        {
            StopCoroutine();
            alertPopup.Hide();
        }

        void StopCoroutine()
        {
            if (showCoroutine != null) { coroutineHelper.StopCoroutine(showCoroutine); }
        }
    }
}