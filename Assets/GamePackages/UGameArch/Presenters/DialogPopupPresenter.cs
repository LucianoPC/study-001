using System;
using UGameArch.Core;
using UGameArch.Helpers;
using UGameArch.UI;
using UnityEngine;

namespace UGameArch.Presenters
{
    public class DialogPopupPresenter
    {
        readonly DialogPopup dialogPopup;
        readonly ICoroutineHelper coroutineHelper;
        
        readonly CanvasGroup canvasGroup;
        
        Coroutine showCoroutine;

        public DialogPopupPresenter(DialogPopup dialogPopup, ICoroutineHelper coroutineHelper)
        {
            this.dialogPopup = dialogPopup;
            this.coroutineHelper = coroutineHelper;
            
            canvasGroup = CanvasGroupHelper.AddCanvasGroup(dialogPopup.gameObject);
        }

        public void Show(string title, string message, string buttonText, bool canBackgroundConfirm, DialogPopup.ButtonType buttonType, Action onConfirm)
        {
            dialogPopup.Show(title, message, buttonText, canBackgroundConfirm, buttonType, onConfirm);
            StopCoroutine();
            showCoroutine = coroutineHelper.StartCoroutine(CanvasGroupHelper.CoroutineFadeIn(canvasGroup));
        }

        public void ShowError(IError error, Action onConfirm)
        {
            dialogPopup.Show(
                title: "Error",
                message: error.Message,
                buttonText: "Ok",
                canBackgroundConfirm: true,
                DialogPopup.ButtonType.Confirm,
                onConfirm: onConfirm);
        }

        public void Hide()
        {
            StopCoroutine();
            dialogPopup.Hide();
        }

        void StopCoroutine()
        {
            if (showCoroutine != null) { coroutineHelper.StopCoroutine(showCoroutine); }
        }
    }
}