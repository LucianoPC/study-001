using System;
using UGameArch.Core;
using UGameArch.Helpers;
using UGameArch.UI;
using UnityEngine;

namespace UGameArch.Presenters
{
    public class TextInputPopupPresenter
    {
        readonly TextInputPopup textInputPopup;
        readonly ICoroutineHelper coroutineHelper;
        
        readonly CanvasGroup canvasGroup;
        
        Coroutine showCoroutine;

        public TextInputPopupPresenter(TextInputPopup textInputPopup, ICoroutineHelper coroutineHelper)
        {
            this.textInputPopup = textInputPopup;
            this.coroutineHelper = coroutineHelper;
            
            canvasGroup = CanvasGroupHelper.AddCanvasGroup(textInputPopup.gameObject);
        }

        public void Show(string title, string inputText, string confirmButtonText, string cancelButtonText,
            bool canBackgroundCancel, Action<TextInputPopup.Result> onConfirm, Action onCancel)
        {
            textInputPopup.Show(title, inputText, confirmButtonText, cancelButtonText, canBackgroundCancel, onConfirm, onCancel);
            showCoroutine = coroutineHelper.StartCoroutine(CanvasGroupHelper.CoroutineFadeIn(canvasGroup));
        }

        public void Hide()
        {
            coroutineHelper.StopCoroutine(showCoroutine);
            textInputPopup.Hide();
        }
    }
}