﻿using TMPro;
using UnityEngine;

namespace UGameArch.UI
{
    public class AlertPopup : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI messageText;

        public void Show(string message)
        {
            messageText.text = message;

            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
