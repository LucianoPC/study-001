using UnityEngine;

namespace UGameArch.UI
{
    [RequireComponent(typeof(RectTransform))]
    public class LoadingView : MonoBehaviour
    {
        [SerializeField] float animationSpeed = 1f;
        [SerializeField] AnimationCurve animationCurve;
        [SerializeField] IconView iconView;
        
        [SerializeField, HideInInspector] Vector2 originalScale;
        [SerializeField, HideInInspector] RectTransform rectTransform;

        float curveDeltaTime = 0f;
        
        void OnValidate()
        {
            rectTransform = GetComponent<RectTransform>();
            originalScale = rectTransform.localScale;
        }

        void Update()
        {
            UpdateScale();
        }

        public void SetIcon(Sprite sprite)
        {
            iconView.SetIcon(sprite);
        }

        void UpdateScale()
        {
            curveDeltaTime += Time.deltaTime * animationSpeed;

            var value = animationCurve.Evaluate(curveDeltaTime);
            var scale = originalScale * value;

            rectTransform.localScale = scale;
        }
    }
}