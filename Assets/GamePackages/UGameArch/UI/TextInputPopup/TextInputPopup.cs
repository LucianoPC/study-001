﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UGameArch.UI
{
    public class TextInputPopup : MonoBehaviour
    {
        [Header("Input")]
        [SerializeField] TMP_InputField inputField;
        
        [Header("Texts")]
        [SerializeField] TextMeshProUGUI titleText;
        [SerializeField] TextMeshProUGUI confirmButtonText;
        [SerializeField] TextMeshProUGUI cancelButtonText;
        
        [Header("Buttons")]
        [SerializeField] Button closeButton;
        [SerializeField] Button confirmButton;
        [SerializeField] Button cancelButton;
        [SerializeField] Button backgroundButton;

        Action<Result> ConfirmPressed;
        Action CancelPressed;
        
        bool canBackgroundCancel;

        void Awake()
        {
            confirmButton.onClick.AddListener(NotifyConfirmPressed);
            closeButton.onClick.AddListener(NotifyCancelPressed);
            cancelButton.onClick.AddListener(NotifyCancelPressed);
            backgroundButton.onClick.AddListener(OnBackgroundButtonPressed);
        }

        void OnDestroy()
        {
            confirmButton.onClick.RemoveListener(NotifyConfirmPressed);
            closeButton.onClick.RemoveListener(NotifyCancelPressed);
            cancelButton.onClick.RemoveListener(NotifyCancelPressed);
            backgroundButton.onClick.RemoveListener(OnBackgroundButtonPressed);
        }

        public void Show(string title, string inputText, string confirmButtonText, string cancelButtonText, bool canBackgroundCancel, Action<Result> onConfirm, Action onCancel)
        {
            titleText.text = title;
            inputField.text = inputText;
            this.confirmButtonText.text = confirmButtonText;
            this.cancelButtonText.text = cancelButtonText;
            this.canBackgroundCancel = canBackgroundCancel;
            
            gameObject.SetActive(true);
            // inputField.Select();
            // inputField.ActivateInputField();
            
            SetEnabled(true);

            ConfirmPressed = onConfirm;
            CancelPressed = onCancel;
        }
        
        public void Hide()
        {
            gameObject.SetActive(false);
            
            inputField.text = string.Empty;
            confirmButtonText.text = "Confirm";
            cancelButtonText.text = "Cancel";
            titleText.text = "Title";
            
            ConfirmPressed = null;
            CancelPressed = null;
        }

        public void SetEnabled(bool enabled)
        {
            inputField.enabled = enabled;
            closeButton.enabled = enabled;
            cancelButton.enabled = enabled;
            confirmButton.enabled = enabled;
            backgroundButton.enabled = enabled;
        }
        
        void OnBackgroundButtonPressed()
        {
            if (canBackgroundCancel)
            {
                NotifyCancelPressed();
            }
        }

        void NotifyConfirmPressed()
        {
            var result = new Result
            {
                InputText = inputField.text,
            };
            
            ConfirmPressed?.Invoke(result);
        }
        
        void NotifyCancelPressed()
        {
            CancelPressed?.Invoke();
        }

        public class Result
        {
            public string InputText;
        }
    }
}
