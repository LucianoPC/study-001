﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UGameArch.UI
{
    public class DialogPopup : MonoBehaviour
    {
        public enum ButtonType
        {
            Confirm,
            Cancel,
        }
        
        [Header("Popup Message")]
        [SerializeField] TextMeshProUGUI uiTitleText;
        [SerializeField] TextMeshProUGUI uiMessageText;
        
        [Header("Confirm Button")]
        [SerializeField] Button uiConfirmButton;
        [SerializeField] TextMeshProUGUI uiConfirmButtonText;
        
        [Header("Cancel Button")]
        [SerializeField] Button uiCancelButton;
        [SerializeField] TextMeshProUGUI uiCancelButtonText;
        
        [Header("Close Button")]
        [SerializeField] Button uiCloseButton;
        [SerializeField] Button uiBackgroundButton;

        Action confirmPressed;
        bool canBackgroundConfirm;

        public void Awake()
        {
            uiConfirmButton.onClick.AddListener(NotifyConfirmPressed);
            uiCloseButton.onClick.AddListener(NotifyConfirmPressed);
            uiCancelButton.onClick.AddListener(NotifyConfirmPressed);
            uiBackgroundButton.onClick.AddListener(OnBackgroundButtonPressed);
        }

        public void OnDestroy()
        {
            uiConfirmButton.onClick.RemoveListener(NotifyConfirmPressed);
            uiCloseButton.onClick.RemoveListener(NotifyConfirmPressed);
            uiCancelButton.onClick.RemoveListener(NotifyConfirmPressed);
            uiBackgroundButton.onClick.RemoveListener(OnBackgroundButtonPressed);
        }

        public void Show(string title, string message, string buttonText, bool canBackgroundConfirm, ButtonType buttonType, Action onConfirm)
        {
            this.canBackgroundConfirm = canBackgroundConfirm;

            uiTitleText.text = title;
            uiMessageText.text = message;

            if (uiConfirmButton != null) { uiConfirmButtonText.text = buttonText; }
            if (uiCancelButton != null) { uiCancelButtonText.text = buttonText; }
            
            uiConfirmButton.gameObject.SetActive(buttonType == ButtonType.Confirm);
            uiCancelButton.gameObject.SetActive(buttonType == ButtonType.Cancel);

            confirmPressed = onConfirm;

            gameObject.SetActive(true);
        }

        public void Hide()
        {
            confirmPressed = null;
            gameObject.SetActive(false);
        }

        void OnBackgroundButtonPressed()
        {
            if (canBackgroundConfirm)
            {
                NotifyConfirmPressed();
            }
        }

        void NotifyConfirmPressed()
        {
            confirmPressed?.Invoke();
        }
    }
}
