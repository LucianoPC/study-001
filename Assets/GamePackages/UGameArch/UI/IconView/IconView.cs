using UnityEngine;
using UnityEngine.UI;

namespace UGameArch.UI
{
    public class IconView : MonoBehaviour
    {
        [SerializeField] Image image;
        [SerializeField] AspectRatioFitter aspectRatioFitter;

        public Color Color
        {
            get => image.color;
            set => image.color = value;
        }
        
        public void SetIcon(Sprite sprite)
        {
            image.sprite = sprite;
            aspectRatioFitter.aspectRatio = sprite.bounds.size.x / sprite.bounds.size.y;
        }
    }
}