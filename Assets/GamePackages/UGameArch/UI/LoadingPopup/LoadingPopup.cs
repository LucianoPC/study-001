using UnityEngine;

namespace UGameArch.UI
{
    public class LoadingPopup : MonoBehaviour
    {
        [SerializeField] LoadingView loadingView;

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void SetIcon(Sprite sprite)
        {
            loadingView.SetIcon(sprite);
        }
    }
}