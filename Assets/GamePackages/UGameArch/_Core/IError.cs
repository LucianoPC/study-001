namespace UGameArch.Core
{
    public interface IError
    {
        string Code { get; }
        string Message { get; }
    }
}