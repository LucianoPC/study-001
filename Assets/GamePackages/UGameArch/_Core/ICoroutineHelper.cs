using System.Collections;
using UnityEngine;

namespace UGameArch.Core
{
    public interface ICoroutineHelper
    {
        Coroutine StartCoroutine(IEnumerator routine);
        void StopCoroutine(Coroutine routine);
    }
}