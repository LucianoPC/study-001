namespace UGameArch.Core
{
    public interface IPersistence
    {
        void Save(string id, object obj);
        T Load<T>(string id) where T : class;
        bool HasId(string id);
    }
}