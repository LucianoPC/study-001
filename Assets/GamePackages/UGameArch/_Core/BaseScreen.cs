using UnityEngine;

namespace UGameArch.Core
{
    public abstract class BaseScreen : MonoBehaviour
    {
        public abstract void Initialize();
    }
}