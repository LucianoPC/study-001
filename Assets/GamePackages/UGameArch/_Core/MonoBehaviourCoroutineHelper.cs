using System.Collections;
using UnityEngine;

namespace UGameArch.Core
{
    public class MonoBehaviourCoroutineHelper : ICoroutineHelper
    {
        readonly MonoBehaviour monoBehaviour;

        bool isAlive;

        public MonoBehaviourCoroutineHelper(MonoBehaviour monoBehaviour)
        {
            this.monoBehaviour = monoBehaviour;

            isAlive = true;
        }

        public Coroutine StartCoroutine(IEnumerator routine)
        {
            return monoBehaviour.StartCoroutine(routine);
        }

        public void StopCoroutine(Coroutine routine)
        {
            if (isAlive && routine != null)
            {
                monoBehaviour.StopCoroutine(routine);
            }
        }

        public void SetDead()
        {
            isAlive = false;
        }

        public void StopAllCoroutines()
        {
            monoBehaviour.StopAllCoroutines();
        }
    }
}