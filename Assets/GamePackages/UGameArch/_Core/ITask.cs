
using System;

namespace UGameArch.Core
{
    public interface ITask
    {
        string Name { get; }

        event Action Finished;
        event Action<IError> Failed;
        
        void Run();
    }
}