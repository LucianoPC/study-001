using UnityEngine;

namespace UGameArch.Core
{
    public class UnityLogger : ILogger
    {
        public Logger.LogLevel LogLevel { get; set; }

        public void LogDebug(string message, params object[] objects)
        {
            if (LogLevel <= Logger.LogLevel.Debug) { Debug.LogFormat(message, objects); }
        }
        
        public void LogInfo(string message, params object[] objects)
        {
            if (LogLevel <= Logger.LogLevel.Info) { Debug.LogFormat(message, objects); }
        }

        public void LogWarning(string message, params object[] objects)
        {
            if (LogLevel <= Logger.LogLevel.Warning) { Debug.LogWarningFormat(message, objects); }
        }

        public void LogError(string message, params object[] objects)
        {
            if (LogLevel <= Logger.LogLevel.Error) { Debug.LogErrorFormat(message, objects); }
        }
    }
}