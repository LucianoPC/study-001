namespace UGameArch.Core
{
    public interface ILogger
    {
        Logger.LogLevel LogLevel { get; set; }
        void LogDebug(string message, params object[] objects);
        void LogInfo(string message, params object[] objects);
        void LogWarning(string message, params object[] objects);
        void LogError(string message, params object[] objects);
    }
}