namespace UGameArch.Core
{
    public static class Logger
    {
        public enum LogLevel
        {
            Error = 0,
            Warning = 1,
            Info = 2,
            Debug = 3,
        }

        static ILogger logger = new UnityLogger();
        static LogLevel logLevel;

        public static void SetLogger(ILogger logger)
        {
            Logger.logger = logger;
        }

        public static void SetLogLevel(LogLevel logLevel)
        {
            Logger.logLevel = logLevel;
        }

        public static void LogDebug(string message, params object[] objects)
        {
            logger.LogDebug(message, objects);
        }

        public static void LogInfo(string message, params object[] objects)
        {
            logger.LogInfo(message, objects);
        }

        public static void LogWarning(string message, params object[] objects)
        {
            logger.LogWarning(message, objects);
        }

        public static void LogError(string message, params object[] objects)
        {
            logger.LogError(message, objects);
        }
    }
}