using UnityEngine;

namespace UGameArch.Core
{
    public abstract class BaseScene : MonoBehaviour
    {
        [SerializeField] ScreenProvider screenProvider;
        [SerializeField] Transform screenParentTransform;
        
        ScreenManager screenManager;
        ScreenManager ScreenManager => screenManager ??= new ScreenManager(screenProvider, screenParentTransform);

        protected T CreateScreen<T>() where T : BaseScreen
        {
            return ScreenManager.CreateScreen<T>();
        }

        protected void SetCurrentScreen(BaseScreen screen)
        {
            screenManager.SetCurrentScreen(screen);
        }

        protected void AddScreen(BaseScreen screen)
        {
            screenManager.AddScreen(screen);
        }

        protected void RemoveScreen(BaseScreen screen)
        {
            screenManager.RemoveScreen(screen);
        }
    }
}