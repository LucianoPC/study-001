using System.Collections.Generic;
using UnityEngine;

namespace UGameArch.Core
{
    public class ScreenManager
    {
        readonly ScreenProvider screenProvider;
        readonly Transform screenParentTransform;

        readonly List<GameObject> screenGameObjects;

        public ScreenManager(ScreenProvider screenProvider, Transform screenParentTransform)
        {
            this.screenProvider = screenProvider;
            this.screenParentTransform = screenParentTransform;

            screenGameObjects = new List<GameObject>();
        }

        public T CreateScreen<T>() where T : BaseScreen
        {
            var screen = screenProvider.GetScreen<T>();
            var screenPrefab = screen.gameObject;
            var screenGameObject = Object.Instantiate(screenPrefab, screenParentTransform);
            screenGameObject.SetActive(false);

            return screenGameObject.GetComponent<T>();
        }

        public void SetCurrentScreen(BaseScreen screen)
        {
            DestroyCurrentScreen();

            screenGameObjects.Add(screen.gameObject);
            
            screen.gameObject.SetActive(true);
        }

        public void AddScreen(BaseScreen screen)
        {
            screenGameObjects.Add(screen.gameObject);
            
            screen.gameObject.SetActive(true);
        }

        public void RemoveScreen(BaseScreen screen)
        {
            screenGameObjects.Remove(screen.gameObject);
            Object.Destroy(screen.gameObject);
        }

        void DestroyCurrentScreen()
        {
            foreach (var screenGameObject in screenGameObjects)
            {
                Object.Destroy(screenGameObject);
            }
            
            screenGameObjects.Clear();
        }
    }
}