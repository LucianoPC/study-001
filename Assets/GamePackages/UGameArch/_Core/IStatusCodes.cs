using System.Collections.Generic;

namespace UGameArch.Core
{
    public interface IStatusCodes
    {
        List<string> SuccessCodes { get; }        
    }
}