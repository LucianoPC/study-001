using System;

namespace UGameArch.Core
{
    public class ModelBox<TModel> where TModel : class, new()
    {
        TModel model;

        public TModel Model => model;

        public event Action Updated;

        public ModelBox() : this(new TModel())
        {
        }

        public ModelBox(TModel model)
        {
            this.model = model;
        }

        public void Set(TModel model)
        {
            this.model = model;
            
            NotifyUpdated();
        }

        public void Set(Action<TModel> action)
        {
            action?.Invoke(model);
            
            NotifyUpdated();
        }

        public void Update()
        {
            NotifyUpdated();
        }

        protected void NotifyUpdated()
        {
            Updated?.Invoke();
        }
    }
}