using System;

namespace UGameArch.Core
{
    public interface IConnection
    {
        bool HasConnect { get; }
        bool HasListen { get; }
        
        void Connect(string host, int port, Action onSuccess, Action<IError> onError);
        void Disconnect();
        void SendRequest(string route, Action onSuccess, Action<IError> onError);
        void SendRequest(string route, object request, Action onSuccess, Action<IError> onError);
        void SendRequest<T>(string route, Action<T> onSuccess, Action<IError> onError);
        void SendRequest<T>(string route, object parameters, Action<T> onSuccess, Action<IError> onError);
        void ListenRoute(string route, Action callback);
        void ListenRoute<T>(string route, Action<T> callback);
        void StopListenRoute(string route);
        void StopListenAllRoutes();
        void ClearRequests();
        void ClearAllRequestsAndStopListenAllRoutes();
    }
}