using UnityEngine;

namespace UGameArch.Core
{
    [CreateAssetMenu(fileName = "ScreenProvider.asset", menuName = "UGameArch/Screen Provider", order = 0)]
    public class ScreenProvider : ScriptableObject
    {
        [SerializeField] BaseScreen[] screens;

        public T GetScreen<T>() where T : BaseScreen
        {
            foreach (var screen in screens)
            {
                if (typeof(T) == screen.GetType())
                {
                    return (T)screen;
                }
            }
            
            Logger.LogError($"Screen {typeof(T)} not found");

            return null;
        }
    }
}