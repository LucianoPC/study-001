namespace UGameArch.Core
{
    public interface ISerializer
    {
        string ToJson(object obj);
        T FromJson<T>(string json);
    }
}