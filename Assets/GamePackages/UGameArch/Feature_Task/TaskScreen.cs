using System;
using System.Collections.Generic;
using UGameArch.Core;
using UGameArch.UI;
using UnityEngine;

namespace UGameArch.Feature.Task
{
    public class TaskScreen : BaseScreen
    {
        [SerializeField] AlertPopup alertPopup;
        [SerializeField] DialogPopup dialogPopup;

        TaskManager taskManager;
        TaskPresenter taskPresenter;

        public event Action Finished;

        public void Setup(IEnumerable<ITask> tasks, ICoroutineHelper coroutineHelper)
        {
            taskManager = new TaskManager();
            taskPresenter = new TaskPresenter(alertPopup, dialogPopup, taskManager, coroutineHelper);

            foreach (var task in tasks)
            {
                taskManager.AddTask(task);
            }
        }

        public override void Initialize()
        {
            taskManager.AllTasksRunFinished += OnAllTasksRunFinished;
            
            taskPresenter.Initialize();
            taskManager.RunTasks();
        }

        void OnDestroy()
        {
            if (taskManager != null)
            {
                taskManager.AllTasksRunFinished -= OnAllTasksRunFinished;
            }
            
            taskPresenter?.Dispose();
        }

        void OnAllTasksRunFinished()
        {
            Finished?.Invoke();
        }
    }
}