using UGameArch.Core;
using UGameArch.Presenters;
using UGameArch.UI;

namespace UGameArch.Feature.Task
{
    public class TaskPresenter
    {
        readonly AlertPopupPresenter alertPopup;
        readonly DialogPopupPresenter dialogPopup;
        readonly TaskManager taskManager;

        public TaskPresenter(AlertPopup alertPopup, DialogPopup dialogPopup, TaskManager taskManager, ICoroutineHelper coroutineHelper)
        {
            this.alertPopup = new AlertPopupPresenter(alertPopup, coroutineHelper);
            this.dialogPopup = new DialogPopupPresenter(dialogPopup, coroutineHelper);
            this.taskManager = taskManager;
        }

        public void Initialize()
        {
            taskManager.TaskRunFailed += OnTaskRunFailed;
            taskManager.TaskRunStarted += OnTaskRunStarted;
            taskManager.AllTasksRunFinished += OnAllTasksRunFinished;
            
            ShowCurrentTask();
        }

        public void Dispose()
        {
            alertPopup.Hide();
            dialogPopup.Hide();
            
            taskManager.TaskRunFailed -= OnTaskRunFailed;
            taskManager.TaskRunStarted -= OnTaskRunStarted;
            taskManager.AllTasksRunFinished -= OnAllTasksRunFinished;
        }

        void OnTaskRunFailed(IError error)
        {
            ShowTaskFailedMessage(error);
        }

        void OnTaskRunStarted(TaskInfo taskInfo)
        {
            ShowCurrentTask();
        }

        void OnAllTasksRunFinished()
        {
            ShowCurrentTask();
        }

        void ShowCurrentTask()
        {
            var message = $"[0/{taskManager.NumberOfTasks}] Loading...";

            if (taskManager.IsRunning)
            {
                var taskInfo = taskManager.GetCurrentTaskInfo();
                message = $"[{taskInfo.Number}/{taskManager.NumberOfTasks}] {taskInfo.Name}...";
            }
            else if (taskManager.Finished)
            {
                message = "Loading...";
            }
            
            alertPopup.Show(message);
        }

        void ShowTaskFailedMessage(IError error)
        {
            alertPopup.Hide();
            dialogPopup.Show(
                title: "Error",
                message: $"{error.Message}",
                buttonText: "Try Again",
                canBackgroundConfirm: false,
                DialogPopup.ButtonType.Confirm,
                onConfirm: () =>
                {
                    dialogPopup.Hide();
                    ShowCurrentTask();
                    taskManager.TryAgainAfterFail();
                });
        }
    }
}