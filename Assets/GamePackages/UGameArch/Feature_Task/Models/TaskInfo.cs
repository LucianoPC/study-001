namespace UGameArch.Feature.Task
{
    public struct TaskInfo
    {
        public string Name { get; private set; }
        public int Number { get; private set; }

        public TaskInfo(string name, int number)
        {
            Name = name;
            Number = number;
        }
    }
}