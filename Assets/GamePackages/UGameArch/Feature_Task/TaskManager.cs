using System;
using System.Collections.Generic;
using UGameArch.Core;

namespace UGameArch.Feature.Task
{
    public class TaskManager
    {
        const int NonTaskIndex = -1;
        
        readonly List<ITask> tasks;

        int currentTaskIndex;
        bool isRunning;
        bool finished;
        bool failed;

        public bool IsRunning => isRunning;
        public bool Finished => finished;
        public int NumberOfTasks => tasks.Count;
        public int CurrentTaskNumber => currentTaskIndex + 1;

        public event Action<TaskInfo> TaskRunStarted;
        public event Action<TaskInfo> TaskRunFinished;
        public event Action<IError> TaskRunFailed;
        public event Action AllTasksRunFinished;

        public TaskManager()
        {
            tasks = new List<ITask>();

            currentTaskIndex = -1;
        }

        public void AddTask(ITask task)
        {
            tasks.Add(task);
        }

        public void RunTasks()
        {
            if (isRunning) { return; }

            failed = false;
            isRunning = true;
            finished = false;
            currentTaskIndex = NonTaskIndex;
            
            TryRunNextTask();
        }

        public void TryAgainAfterFail()
        {
            if (!failed) { return; }
            failed = false;
            
            TryRunCurrentTask();
        }

        public TaskInfo GetCurrentTaskInfo()
        {
            var task = GetCurrentTask();
            
            return new TaskInfo(task.Name, CurrentTaskNumber);
        }

        void TryRunCurrentTask()
        {
            if (!HasCurrentTask()) { return; }

            var task = GetCurrentTask();
            
            RunTask(task);
        }

        bool HasCurrentTask()
        {
            return currentTaskIndex != NonTaskIndex;
        }

        void TryRunNextTask()
        {
            if (!HasNextTask())
            {
                FinishRunAllTasks();
                return;
            }

            var task = GetNextTask();
            
            RunTask(task);
        }

        bool HasNextTask()
        {
            var nextTaskIndex = currentTaskIndex + 1;
            var hasNextTask = tasks.Count > nextTaskIndex;
            
            return hasNextTask;
        }

        ITask GetNextTask()
        {
            currentTaskIndex += 1;

            return GetCurrentTask();
        }

        ITask GetCurrentTask()
        {
            return tasks[currentTaskIndex];
        }

        void RunTask(ITask task)
        {
            task.Finished += OnTaskFinished;
            task.Failed += OnTaskFailed;
            
            TaskRunStarted?.Invoke(new TaskInfo(task.Name, CurrentTaskNumber));
            
            task.Run();
        }

        void OnTaskFinished()
        {
            var task = GetCurrentTask();
            task.Finished -= OnTaskFinished;
            task.Failed -= OnTaskFailed;
            
            Logger.LogDebug($"TaskManager | Task {task.Name} finished");

            TaskRunFinished?.Invoke(new TaskInfo(task.Name, CurrentTaskNumber));
            TryRunNextTask();
        }

        void OnTaskFailed(IError error)
        {
            failed = true;
            
            var task = GetCurrentTask();
            task.Finished -= OnTaskFinished;
            task.Failed -= OnTaskFailed;
            
            Logger.LogDebug($"TaskManager | Task {task.Name} failed :: [code: {error.Code}] [message: {error.Message}]");
            
            TaskRunFailed?.Invoke(error);
        }
        
        void FinishRunAllTasks()
        {
            isRunning = false;
            finished = true;
            
            Logger.LogDebug($"TaskManager | All tasks finished");
            
            AllTasksRunFinished?.Invoke();
        }
    }
}