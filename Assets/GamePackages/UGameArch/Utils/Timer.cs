using System;
using System.Collections;
using UGameArch.Core;
using UnityEngine;

namespace UGameArch.Utils
{
    public class Timer
    {
        readonly ICoroutineHelper coroutineHelper;

        Coroutine coroutine;
        TimeSpan remainingTime;

        public TimeSpan RemainingTime => remainingTime;

        public event Action Updated;
        public event Action Finished;

        public Timer(ICoroutineHelper coroutineHelper)
        {
            this.coroutineHelper = coroutineHelper;
        }

        public void Start(DateTime stopAt)
        {
            Stop();
            coroutineHelper.StartCoroutine(CoroutineStart(stopAt));
        }

        public void Stop()
        {
            if (coroutine != null)
            {
                coroutineHelper.StopCoroutine(coroutine);
            }
        }

        IEnumerator CoroutineStart(DateTime stopAt)
        {
            do
            {
                var dateTimeNow = DateTime.UtcNow;

                if (dateTimeNow > stopAt)
                {
                    Finished?.Invoke();
                    yield break;
                }

                remainingTime = stopAt - dateTimeNow;
                
                Updated?.Invoke();

                yield return new WaitForSeconds(1f);

            } while (true);
        }
    }
}