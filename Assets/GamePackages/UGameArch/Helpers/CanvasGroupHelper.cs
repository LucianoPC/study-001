using System;
using System.Collections;
using UnityEngine;

namespace UGameArch.Helpers
{
    public static class CanvasGroupHelper
    {
        const float fadeInTime = 0.125f;
        const float fadeOutTime = 0.125f;

        public static CanvasGroup AddCanvasGroup(GameObject gameObject)
        {
            var hasCanvasGroup = gameObject.TryGetComponent(out CanvasGroup canvasGroup);
            if (!hasCanvasGroup) { canvasGroup = gameObject.gameObject.AddComponent<CanvasGroup>(); }

            return canvasGroup;
        }

        public static IEnumerator CoroutineFadeIn(CanvasGroup canvasGroup, Action onFinish = null)
        {
            return CoroutineFadeIn(fadeInTime, canvasGroup, onFinish);
        }
        
        public static IEnumerator CoroutineFadeIn(float fadeTime, CanvasGroup canvasGroup, Action onFinish = null)
        {
            var startTime = Time.time;

            canvasGroup.alpha = 0f;
            
            while (canvasGroup.alpha < 1f)
            {
                var deltaTime = Time.time - startTime;
                canvasGroup.alpha = deltaTime / fadeTime;
                yield return null;
            }
            
            onFinish?.Invoke();
        }

        public static IEnumerator CoroutineFadeOut(CanvasGroup canvasGroup, Action onFinish = null)
        {
            return CoroutineFadeOut(fadeOutTime, canvasGroup, onFinish);
        }
        
        public static IEnumerator CoroutineFadeOut(float fadeTime, CanvasGroup canvasGroup, Action onFinish = null)
        {
            var startTime = Time.time;
            
            canvasGroup.alpha = 0f;
            
            do
            {
                yield return null;

                var deltaTime = Time.time - startTime;

                canvasGroup.alpha = (fadeTime - deltaTime) / fadeTime;

            } while (canvasGroup.alpha > 0f);
            
            onFinish?.Invoke();
        }
    }
}