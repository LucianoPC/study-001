using System;

namespace UGameArch.Helpers
{
    public static class TimeHelper
    {
        public static TimeSpan GetRemainingTime(DateTime stopAt)
        {
            var dateTimeNow = DateTime.UtcNow;

            if (dateTimeNow > stopAt)
            {
                return TimeSpan.Zero;
            }

            return stopAt - dateTimeNow;
        }
    }
}