using UnityEngine;

namespace Game.UI.Views
{
    public class LeaderboardView : MonoBehaviour
    {
        [Header("Reward Item View")]
        [SerializeField] GameObject leaderboardItemViewPrefab;
        [SerializeField] Transform leaderboardItemViewParent;

        public void SetLeaderboardItems(LeaderboardItem[] leaderboardItems)
        {
            var children = leaderboardItemViewParent.GetComponentsInChildren<LeaderboardItemView>();
            foreach (var child in children) { Destroy(child.gameObject); }

            foreach (var leaderboardItem in leaderboardItems)
            {
                var leaderboardItemView = Instantiate(leaderboardItemViewPrefab, leaderboardItemViewParent).GetComponent<LeaderboardItemView>();
                
                leaderboardItem.SetLeaderboardItemView(leaderboardItemView);
            }
        }
        public class LeaderboardItem
        {
            public string Position;
            public string Name;
            public string BestTime;
            public bool IsHighlighted;

            public void SetLeaderboardItemView(LeaderboardItemView view)
            {
                view.SetPosition(Position);
                view.SetName(Name);
                view.SetBestTime(BestTime);
                view.SetHighlighted(IsHighlighted);
            }
        }
    }
}