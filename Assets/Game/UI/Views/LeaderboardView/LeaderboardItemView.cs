using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Views
{
    public class LeaderboardItemView : MonoBehaviour
    {
        [SerializeField] Image backgroundImage;
        [SerializeField] Image highlightBackgroundImage;
        [SerializeField] TextMeshProUGUI positionText;
        [SerializeField] TextMeshProUGUI nameText;
        [SerializeField] TextMeshProUGUI bestTimeText;

        public void SetPosition(string position)
        {
            positionText.text = position;
        }

        public void SetName(string name)
        {
            nameText.text = name;
        }

        public void SetBestTime(string bestTime)
        {
            bestTimeText.text = bestTime;
        }

        public void SetHighlighted(bool isHighlighted)
        {
            backgroundImage.gameObject.SetActive(!isHighlighted);
            highlightBackgroundImage.gameObject.SetActive(isHighlighted);
        }
    }
}