using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Views.TopBar
{
    public class TopBarWithBackButtonView : MonoBehaviour
    {
        [SerializeField] Button backButton;
        [SerializeField] TextMeshProUGUI titleText;
        [SerializeField] TopBarItemView softCurrencyView;
        [SerializeField] TopBarItemView hardCurrencyView;

        public event Action BackButtonPressed;

        public TopBarItemView SoftCurrencyView => softCurrencyView;
        public TopBarItemView HardCurrencyView => hardCurrencyView;

        void Awake()
        {
            backButton.onClick.AddListener(NotifyBackButtonPressed);
        }

        void OnDestroy()
        {
            backButton.onClick.RemoveListener(NotifyBackButtonPressed);
        }

        public void SetTitle(string title)
        {
            titleText.text = title;
        }

        void NotifyBackButtonPressed()
        {
            BackButtonPressed?.Invoke();
        }
    }
}