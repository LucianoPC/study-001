using UnityEngine;

namespace Game.UI.Views.TopBar
{
    public class TopBarCurrenciesView : MonoBehaviour
    {
        [SerializeField] TopBarItemView softCurrencyView;
        [SerializeField] TopBarItemView hardCurrencyView;

        public TopBarItemView SoftCurrencyView => softCurrencyView;
        public TopBarItemView HardCurrencyView => hardCurrencyView;
    }
}