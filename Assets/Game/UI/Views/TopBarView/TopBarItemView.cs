using System;
using TMPro;
using UGameArch.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Views.TopBar
{
    public class TopBarItemView : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI amountText;
        [SerializeField] Button selfButton;
        [SerializeField] IconView iconView;

        public event Action Clicked;

        void Awake()
        {
            selfButton.onClick.AddListener(NotifyPressed);
        }

        void OnDestroy()
        {
            selfButton.onClick.RemoveListener(NotifyPressed);
        }

        public void SetIcon(Sprite sprite)
        {
            iconView.SetIcon(sprite);
        }

        public void SetText(string text)
        {
            amountText.text = text;
        }

        void NotifyPressed()
        {
            Clicked?.Invoke();
        }
    }
}