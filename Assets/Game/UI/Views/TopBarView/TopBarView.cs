using UnityEngine;

namespace Game.UI.Views.TopBar
{
    public class TopBarView : MonoBehaviour
    {
        [SerializeField] TopBarItemView playerNameView;
        [SerializeField] TopBarItemView softCurrencyView;
        [SerializeField] TopBarItemView hardCurrencyView;

        public TopBarItemView PlayerNameView => playerNameView;
        public TopBarItemView SoftCurrencyView => softCurrencyView;
        public TopBarItemView HardCurrencyView => hardCurrencyView;
    }
}