using System;
using TMPro;
using UGameArch.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Views
{
    public class StatusView : MonoBehaviour
    {
        public enum State
        {
            LevelUp,
            SpeedUp,
            NoLevelUp,
        }
        
        [SerializeField] TextMeshProUGUI levelText;
        [SerializeField] TextMeshProUGUI speedText;
        [SerializeField] TextMeshProUGUI nextSpeedText;
        [SerializeField] TextMeshProUGUI speedUpTimeText;
        [SerializeField] Button levelUpButton;
        [SerializeField] Button speedUpButton;
        [SerializeField] TextMeshProUGUI levelUpCostText;
        [SerializeField] TextMeshProUGUI speedUpCostText;
        [SerializeField] IconView levelUpCostIconView;
        [SerializeField] IconView speedUpCostIconView;

        public event Action LevelUpPressed;
        public event Action SpeedUpPressed;

        void Awake()
        {
            levelUpButton.onClick.AddListener(NotifyLevelUpPressed);
            speedUpButton.onClick.AddListener(NotifySpeedUpPressed);
        }

        void OnDestroy()
        {
            levelUpButton.onClick.RemoveListener(NotifyLevelUpPressed);
            speedUpButton.onClick.RemoveListener(NotifySpeedUpPressed);
        }

        public void SetState(State state)
        {
            levelUpButton.gameObject.SetActive(state == State.LevelUp);
            speedUpButton.gameObject.SetActive(state == State.SpeedUp);
        }

        public void SetLevelText(string text)
        {
            levelText.text = text;
        }

        public void SetSpeedText(string text)
        {
            speedText.text = text;
        }

        public void SetNextSpeedText(string text)
        {
            nextSpeedText.text = text;
        }

        public void SetSpeedUpTimeText(string text)
        {
            speedUpTimeText.text = text;
        }

        public void SetLevelUpCostText(string text)
        {
            levelUpCostText.text = text;
        }

        public void SetSpeedUpCostText(string text)
        {
            speedUpCostText.text = text;
        }

        public void SetLevelUpCostIconView(Sprite icon)
        {
            levelUpCostIconView.SetIcon(icon);
        }

        public void SetSpeedUpCostIconView(Sprite icon)
        {
            speedUpCostIconView.SetIcon(icon);
        }

        public void SetSpeedUpCostIconViewVisible(bool isVisible)
        {
            speedUpCostIconView.gameObject.SetActive(isVisible);
        }

        void NotifyLevelUpPressed()
        {
            LevelUpPressed?.Invoke();
        }

        void NotifySpeedUpPressed()
        {
            SpeedUpPressed?.Invoke();
        }
    }
}