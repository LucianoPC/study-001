using System;
using Game.Presenters;
using Game.Repositories;
using Game.UI.Views;
using Game.UI.Views.TopBar;
using UGameArch.Core;
using UGameArch.Helpers;
using UGameArch.Providers;
using UnityEngine;

namespace Game.UI.Screens
{
    public class LeaderboardScreen : BaseScreen
    {
        [SerializeField] TopBarWithBackButtonView topBarView;
        [SerializeField] LeaderboardView leaderboardView;
        [SerializeField] CanvasGroup canvasGroup;

        TopBarWithBackButtonPresenter topBarPresenter;
        LeaderboardPresenter leaderboardPresenter;
        
        ICoroutineHelper coroutineHelper;
        
        Coroutine showCoroutine;
        
        public event Action Back
        {
            add => topBarPresenter.BackButtonPressed += value;
            remove => topBarPresenter.BackButtonPressed -= value;
        }

        public void Setup(
            ModelBoxRepository modelBoxes,
            SpriteProvider spriteProvider,
            ICoroutineHelper coroutineHelper)
        {
            this.coroutineHelper = coroutineHelper;
            
            topBarPresenter =
                new TopBarWithBackButtonPresenter("Leaderboard", modelBoxes.WalletInfoBox, topBarView, spriteProvider);

            leaderboardPresenter =
                new LeaderboardPresenter(modelBoxes.PlayerInfoBox, modelBoxes.LeaderboardInfoBox, leaderboardView);
        }
        
        public override void Initialize()
        {
            topBarPresenter.Initialize();
            leaderboardPresenter.Initialize();
            
            showCoroutine = coroutineHelper.StartCoroutine(CanvasGroupHelper.CoroutineFadeIn(canvasGroup));
        }

        void OnDestroy()
        {
            topBarPresenter?.Dispose();
            leaderboardPresenter?.Dispose();
            
            coroutineHelper.StopCoroutine(showCoroutine);
        }
    }
}