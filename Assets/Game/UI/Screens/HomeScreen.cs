using System;
using Game.Presenters;
using Game.Repositories;
using Game.Services;
using Game.UI.Views;
using Game.UI.Views.TopBar;
using UGameArch.Core;
using UGameArch.Helpers;
using UGameArch.Providers;
using UGameArch.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Screens
{
    public class HomeScreen : BaseScreen
    {
        [SerializeField] TopBarView topBarView;
        [SerializeField] StatusView statusView;
        [SerializeField] DialogPopup dialogPopup;
        [SerializeField] LoadingPopup loadingPopup;
        [SerializeField] TextInputPopup textInputPopup;
        [SerializeField] Button playButton;
        [SerializeField] Button leaderboardButton;
        [SerializeField] Canvas canvas;
        [SerializeField] CanvasGroup canvasGroup;

        TopBarPresenter topBarPresenter;
        StatusPresenter statusPresenter;
        
        ICoroutineHelper coroutineHelper;

        public event Action PlayPressed;
        public event Action LeaderboardPressed;
        
        public void Setup(
            Camera camera,
            ModelBoxRepository modelBoxes,
            PlayerService playerService,
            CharacterService characterService,
            SpriteProvider spriteProvider,
            ICoroutineHelper coroutineHelper)
        {
            this.coroutineHelper = coroutineHelper;

            canvas.worldCamera = camera;
            canvas.renderMode = RenderMode.ScreenSpaceCamera;

            topBarPresenter = new TopBarPresenter(modelBoxes.PlayerInfoBox, modelBoxes.WalletInfoBox,
                topBarView, dialogPopup, loadingPopup, textInputPopup, playerService, spriteProvider, coroutineHelper);

            statusPresenter = new StatusPresenter(modelBoxes.PlayerInfoBox, modelBoxes.CharacterInfoBox,
                modelBoxes.CharacterUpgradeInfoBox, statusView, dialogPopup, loadingPopup, characterService,
                spriteProvider, coroutineHelper);
        }
       
        public override void Initialize()
        {
            playButton.onClick.AddListener(NotifyPlayPressed);
            leaderboardButton.onClick.AddListener(NotifyLeaderboardPressed);
            
            topBarPresenter.Initialize();
            statusPresenter.Initialize();
            
            coroutineHelper.StartCoroutine(CanvasGroupHelper.CoroutineFadeIn(canvasGroup));
        }

        void OnDestroy()
        {
            playButton.onClick.RemoveListener(NotifyPlayPressed);
            leaderboardButton.onClick.RemoveListener(NotifyLeaderboardPressed);
            
            topBarPresenter?.Dispose();
            statusPresenter?.Dispose();
        }

        void NotifyPlayPressed()
        {
            PlayPressed?.Invoke();
        }

        void NotifyLeaderboardPressed()
        {
            LeaderboardPressed?.Invoke();
        }
    }
}