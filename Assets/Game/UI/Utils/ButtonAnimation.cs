using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.UI.Utils
{
    [RequireComponent(typeof(Button))]
    public class ButtonAnimation : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler
    {
        const float ScaleMultiplier = 0.95f;
        
        [SerializeField, HideInInspector] Vector3 originalScale;
        [SerializeField, HideInInspector] RectTransform rectTransform;

        bool isPointerDown;

        void OnValidate()
        {
            rectTransform = GetComponent<RectTransform>();
            originalScale = rectTransform.localScale;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!eventData.eligibleForClick) { return; }
            
            isPointerDown = true;
            MultiplyScale();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!eventData.eligibleForClick) { return; }
            
            isPointerDown = false;
            RestoreScale();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!eventData.eligibleForClick) { return; }
            
            RestoreScale();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!eventData.eligibleForClick) { return; }
            
            if (isPointerDown)
            {
                MultiplyScale();
            }
        }

        void MultiplyScale()
        {
            rectTransform.localScale = originalScale * ScaleMultiplier;
        }

        void RestoreScale()
        {
            rectTransform.localScale = originalScale;
        }
    }
}