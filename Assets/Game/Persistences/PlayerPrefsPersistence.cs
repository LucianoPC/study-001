using System;
using UGameArch.Core;
using UnityEngine;
using Logger = UGameArch.Core.Logger;

namespace Game.Persistences
{
    public class PlayerPrefsPersistence : IPersistence
    {
        readonly bool hasCache;
        readonly ISerializer serializer;

        public PlayerPrefsPersistence(ISerializer serializer, bool hasCache = true)
        {
            this.serializer = serializer;
            this.hasCache = hasCache;
        }

        public void Save(string id, object obj)
        {
            if (!hasCache) { return; }
            
            var json = serializer.ToJson(obj);
            
            PlayerPrefs.SetString(id, json);
            PlayerPrefs.Save();
        }

        public T Load<T>(string id) where T : class
        {
            if (!hasCache) { return null; }
            
            var json = PlayerPrefs.GetString(id, string.Empty);

            if (string.IsNullOrEmpty(json))
            {
                Logger.LogError($"PlayerPrefsPersistence.Load :: {id} is null or empty");
                return null;
            }
            
            try
            {
                var obj = serializer.FromJson<T>(json);
                return obj;
            }
            catch (Exception e)
            {
                Logger.LogError($"PlayerPrefsPersistence.Load :: {id} deserialize error {e.Message}]");
            }
            
            return null;
        }

        public bool HasId(string id)
        {
            if (!hasCache) { return false; }

            return PlayerPrefs.HasKey(id);
        }
    }
}