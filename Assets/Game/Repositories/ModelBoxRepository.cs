using System;
using System.Collections.Generic;
using Game.Models;
using Game.Models.Boxes;
using UGameArch.Core;
using UnityEngine;
using CharacterInfo = Game.Models.CharacterInfo;

namespace Game.Repositories
{
    public class ModelBoxRepository
    {
        const string keyPlayerInfo = "modelboxrepository::playerinfo";
        const string keyWalletInfo = "modelboxrepository::walletinfo";
        const string keyCharacterInfos = "modelboxrepository::characterinfos";
        
        readonly PlayerInfoBox playerInfoBox;
        readonly WalletInfoBox walletInfoBox;
        readonly CharacterInfoBox characterInfoBox;
        readonly CharacterUpgradeInfoBox characterUpgradeInfoBox;
        readonly GameplayInfoBox gameplayInfoBox;
        readonly PlayFabAccountInfoBox playFabAccountInfoBox;
        readonly LeaderboardInfoBox leaderboardInfoBox;
        
        public PlayerInfoBox PlayerInfoBox => playerInfoBox;
        public WalletInfoBox WalletInfoBox => walletInfoBox;
        public CharacterInfoBox CharacterInfoBox => characterInfoBox;
        public CharacterUpgradeInfoBox CharacterUpgradeInfoBox => characterUpgradeInfoBox;
        public GameplayInfoBox GameplayInfoBox => gameplayInfoBox;
        public PlayFabAccountInfoBox PlayFabAccountInfoBox => playFabAccountInfoBox;
        public LeaderboardInfoBox LeaderboardInfoBox => leaderboardInfoBox;

        IPersistence persistence;

        public ModelBoxRepository(IPersistence persistence)
        {
            this.persistence = persistence;

            var playerInfo = LoadPlayerInfo();
            var walletInfo = LoadWalletInfo();
            var characterInfos = LoadCharacterInfos();

            var characterUpgradeInfos = new List<CharacterUpgradeInfo>
            {
                GetCharacterBlueUpgradeInfo(),
            };

            var gameplayInfo = new GameplayInfo
            {
                loseRewards = new RewardInfo[]
                {
                    new RewardInfo{code = Constants.Code.Currency.Soft, amount = 10},
                },
                winRewards = new RewardInfo[]
                {
                    new RewardInfo{code = Constants.Code.Currency.Soft, amount = 50},
                    new RewardInfo{code = Constants.Code.Currency.Hard, amount = 2},
                },
            };

            playerInfoBox = new PlayerInfoBox(playerInfo);
            walletInfoBox = new WalletInfoBox(walletInfo);
            characterInfoBox = new CharacterInfoBox(characterInfos);
            characterUpgradeInfoBox = new CharacterUpgradeInfoBox(characterUpgradeInfos);
            gameplayInfoBox = new GameplayInfoBox(gameplayInfo);
            playFabAccountInfoBox = new PlayFabAccountInfoBox();
            leaderboardInfoBox = new LeaderboardInfoBox();

            playerInfoBox.Updated += SavePlayerInfo;
            walletInfoBox.Updated += SaveWalletInfo;
            characterInfoBox.Updated += SaveCharacterInfos;

            playerInfoBox.Update();
            walletInfoBox.Update();
            characterInfoBox.Update();
        }
        
        PlayerInfo LoadPlayerInfo()
        {
            if (persistence.HasId(keyPlayerInfo))
            {
                return persistence.Load<PlayerInfo>(keyPlayerInfo);
            }
            
            return new PlayerInfo
            {
                userId = Guid.NewGuid().ToString("D"),
                name = $"Player#{Guid.NewGuid().ToString("D").Substring(0, 8)}",
                selectedCharacterId = Constants.CharacterId.Blue,
                bestTimeSeconds = double.MaxValue,
            };
        }

        void SavePlayerInfo()
        {
            persistence.Save(keyPlayerInfo, playerInfoBox.Model);
        }
        
        WalletInfo LoadWalletInfo()
        {
            if (persistence.HasId(keyWalletInfo))
            {
                return persistence.Load<WalletInfo>(keyWalletInfo);
            }
            
            return new WalletInfo
            {
                currencyMap = new Dictionary<string, long>
                {
                    {Constants.Code.Currency.Hard, 10},
                    {Constants.Code.Currency.Soft, 100},
                },
            };
        }

        void SaveWalletInfo()
        {
            persistence.Save(keyWalletInfo, walletInfoBox.Model);
        }

        List<CharacterInfo> LoadCharacterInfos()
        {
            if (persistence.HasId(keyCharacterInfos))
            {
                return persistence.Load<List<CharacterInfo>>(keyCharacterInfos);
            }
            
            return new List<CharacterInfo>
            {
                new CharacterInfo
                {
                    id = Constants.CharacterId.Blue,
                    level = 1,
                    isUpgrading = false,
                    unixUpgradedAt = 0,
                },
            };
        }

        void SaveCharacterInfos()
        {
            persistence.Save(keyCharacterInfos, characterInfoBox.Model);
        }
        
        CharacterUpgradeInfo GetCharacterBlueUpgradeInfo()
        {
            return new CharacterUpgradeInfo
            {
                characterId = Constants.CharacterId.Blue,
                upgradeItems = new List<CharacterUpgradeItemInfo>
                {
                    // Level 1
                    new CharacterUpgradeItemInfo
                    {
                        speed = 10f,
                        levelUpCost = 0,
                        speedUpCost = 0,
                        speedUpSeconds = 0,
                        levelUpCurrencyCode = Constants.Code.Currency.Soft,
                        speedUpCurrencyCode = Constants.Code.Currency.Hard,
                    },
                    
                    // Level 2
                    new CharacterUpgradeItemInfo
                    {
                        speed = 11f,
                        levelUpCost = 20,
                        speedUpCost = 0,
                        speedUpSeconds = 0,
                        levelUpCurrencyCode = Constants.Code.Currency.Soft,
                        speedUpCurrencyCode = Constants.Code.Currency.Hard,
                    },
                    
                    // Level 3
                    new CharacterUpgradeItemInfo
                    {
                        speed = 14f,
                        levelUpCost = 40,
                        speedUpCost = 1,
                        speedUpSeconds = 10,
                        levelUpCurrencyCode = Constants.Code.Currency.Soft,
                        speedUpCurrencyCode = Constants.Code.Currency.Hard,
                    },
                    
                    // Level 4
                    new CharacterUpgradeItemInfo
                    {
                        speed = 15f,
                        levelUpCost = 75,
                        speedUpCost = 5,
                        speedUpSeconds = 60,
                        levelUpCurrencyCode = Constants.Code.Currency.Soft,
                        speedUpCurrencyCode = Constants.Code.Currency.Hard,
                    },
                    
                    // Level 4
                    new CharacterUpgradeItemInfo
                    {
                        speed = 20f,
                        levelUpCost = 120,
                        speedUpCost = 20,
                        speedUpSeconds = 90,
                        levelUpCurrencyCode = Constants.Code.Currency.Soft,
                        speedUpCurrencyCode = Constants.Code.Currency.Hard,
                    },
                },
            };
        }
    }
}