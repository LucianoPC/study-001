using Game.Gameplay;
using Game.Gameplay.UI.Screens;
using Game.Models;
using Game.Repositories;
using Game.RewardClaimers;
using UGameArch.Core;
using UGameArch.Providers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scenes.Gameplay
{
    public class GameplayScene : BaseScene
    {
        [SerializeField] GameManager gameManager;

        GameplayServices services;
        ModelBoxRepository modelBoxes;
        SpriteProvider spriteProvider;
        RewardClaimer rewardClaimer;
        ICoroutineHelper coroutineHelper;

        public void Initialize(
            ModelBoxRepository modelBoxes,
            GameplayServices services,
            SpriteProvider spriteProvider,
            RewardClaimer rewardClaimer,
            ICoroutineHelper coroutineHelper)
        {
            this.modelBoxes = modelBoxes;
            this.services = services;
            this.spriteProvider = spriteProvider;
            this.rewardClaimer = rewardClaimer;
            this.coroutineHelper = coroutineHelper;
            
            ShowGameplayScreen();
            gameManager.Initialize(modelBoxes.PlayerInfoBox.Model.bestTimeSeconds, GetAttributes());
            gameManager.WonGame += OnWonGame;
            gameManager.LostGame += OnLostGame;
        }

        void OnWonGame(GameResult gameResult)
        {
            gameManager.WonGame -= OnWonGame;
            gameManager.LostGame -= OnLostGame;
            
            var rewardInfos = modelBoxes.GameplayInfoBox.Model.winRewards;
            
            ClaimRewards(rewardInfos);
            
            services.Player.SetBestTime(gameManager.GameTimer.TotalSeconds,
                onSuccess: () =>
                {
                    ShowGameplayWinScreen(gameResult, rewardInfos);
                },
                onError: error =>
                {
                    
                });
        }

        void OnLostGame(GameResult gameResult)
        {
            gameManager.WonGame -= OnWonGame;
            gameManager.LostGame -= OnLostGame;

            var rewardInfos = modelBoxes.GameplayInfoBox.Model.loseRewards;
            
            ClaimRewards(rewardInfos);
            
            ShowShowGameplayLoseScreen(gameResult, rewardInfos);
        }

        void ShowGameplayScreen()
        {
            var screen = CreateScreen<GameplayScreen>();
            screen.Setup(gameManager.GameStateMachine, gameManager.GameTimer, coroutineHelper);
            screen.Initialize();
            SetCurrentScreen(screen);
        }

        void ShowGameplayWinScreen(GameResult gameResult, RewardInfo[] rewardInfos)
        {
            var screen = CreateScreen<GameplayWinScreen>();
            screen.Setup(gameResult.IsNewHighScore, gameResult.Time, gameResult.BestTime, modelBoxes.WalletInfoBox,
                spriteProvider, rewardInfos, coroutineHelper);
            screen.QuitPressed += () =>
            {
                LoadMainMenuScene();
            };
            screen.PlayAgainPressed += () =>
            {
                LoadGameplayScene();
            };
            screen.Initialize();
            SetCurrentScreen(screen);
        }

        void ShowShowGameplayLoseScreen(GameResult gameResult, RewardInfo[] rewardInfos)
        {
            var screen = CreateScreen<GameplayLoseScreen>();
            screen.Setup(gameResult.Time, gameResult.BestTime, modelBoxes.WalletInfoBox, spriteProvider, rewardInfos,
                coroutineHelper);
            screen.QuitPressed += () =>
            {
                LoadMainMenuScene();
            };
            screen.PlayAgainPressed += () =>
            {
                LoadGameplayScene();
            };
            screen.Initialize();
            SetCurrentScreen(screen);
        }

        void LoadMainMenuScene()
        {
            SceneManager.LoadScene(Constants.SceneName.MainMenu);
        }

        void LoadGameplayScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        GamePlayerAttributes GetAttributes()
        {
            var characterId = modelBoxes.PlayerInfoBox.Model.selectedCharacterId;
            var characterInfo = modelBoxes.CharacterInfoBox.GetCharacterInfo(characterId);
            
            var upgradeIndex = characterInfo.level - 1;
            var upgrades = modelBoxes.CharacterUpgradeInfoBox.GetCharacterUpgradeInfo(characterId).upgradeItems;
            
            var currentUpgrade = upgrades[upgradeIndex];

            return new GamePlayerAttributes
            {
                speed = currentUpgrade.speed,
            };
        }

        void ClaimRewards(RewardInfo[] rewardInfos)
        {
            foreach (var rewardInfo in rewardInfos)
            {
                rewardClaimer.ClaimReward(rewardInfo.code, rewardInfo.amount);
            }
        }
    }
}