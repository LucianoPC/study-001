using Game.Services;

namespace Game.Scenes.Gameplay
{
    public class GameplayServices
    {
        readonly PlayerService playerService;

        public PlayerService Player => playerService;

        public GameplayServices(PlayerService playerService)
        {
            this.playerService = playerService;
        }
    }
}