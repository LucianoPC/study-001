using Game.Persistences;
using Game.Repositories;
using Game.RewardClaimers;
using Game.Scenes.Gameplay;
using Game.Scenes.MainMenu;
using Game.Serializers;
using Game.Services;
using UGameArch.Core;
using UGameArch.Providers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scenes
{
    public class SceneInitializer : MonoBehaviour
    {
        static SceneInitializer instance;
        
        [SerializeField] SpriteProvider spriteProvider;

        bool initialized;
        bool isFirstInitialization;
        
        ISerializer serializer;
        IPersistence persistence;
        PlayerService playerService;
        CharacterService characterService;
        PlayFabService playFabService;
        LeaderboardService leaderboardService;
        RewardClaimer rewardClaimer;
        ModelBoxRepository modelBoxes;
        
        MonoBehaviourCoroutineHelper coroutineHelper;
        
        void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(gameObject);
                return;
            }

            instance = this;
            DontDestroyOnLoad(gameObject);

            SceneManager.activeSceneChanged += OnActiveSceneChanged;

            isFirstInitialization = true;
            
            serializer = new SimpleJsonSerializer();
            persistence = new PlayerPrefsPersistence(serializer, hasCache: true);
            
            modelBoxes = new ModelBoxRepository(persistence);

            playFabService = new PlayFabService(modelBoxes.PlayerInfoBox, modelBoxes.PlayFabAccountInfoBox);
            playerService = new PlayerService(modelBoxes.PlayerInfoBox, playFabService);
            characterService = new CharacterService(modelBoxes.WalletInfoBox, modelBoxes.CharacterInfoBox, modelBoxes.CharacterUpgradeInfoBox);
            leaderboardService = new LeaderboardService(modelBoxes.LeaderboardInfoBox, playFabService);

            rewardClaimer = new RewardClaimer(modelBoxes.WalletInfoBox);
            
            coroutineHelper = new MonoBehaviourCoroutineHelper(this);
        }

        void Start()
        {
            initialized = true;
            
            LoadScene();
        }

        void OnActiveSceneChanged(Scene arg0, Scene arg1)
        {
            if (!initialized) { return; }
            
            coroutineHelper.StopAllCoroutines();
            LoadScene();
        }

        void OnDestroy()
        {
            if (!initialized) { return; }
            
            coroutineHelper.SetDead();
        }

        void LoadScene()
        {
            var scene = FindObjectOfType<BaseScene>();
            var sceneType = scene.GetType();

            if (sceneType == typeof(GameplayScene))
            {
                InitializeGameplayScene((GameplayScene) scene);
            }
            else if (sceneType == typeof(MainMenuScene))
            {
                InitializeMainMenuScene((MainMenuScene) scene);
            }

            isFirstInitialization = false;
        }

        void InitializeGameplayScene(GameplayScene scene)
        {
            var services = new GameplayServices(playerService);
            
            scene.Initialize(modelBoxes, services, spriteProvider, rewardClaimer, coroutineHelper);
        }

        void InitializeMainMenuScene(MainMenuScene scene)
        {
            var services = new MainMenuServices(playerService, characterService, playFabService, leaderboardService);
            
            scene.Initialize(isFirstInitialization, modelBoxes, services, spriteProvider, coroutineHelper);
        }
    }
}