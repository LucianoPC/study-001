using Game.Services;

namespace Game.Scenes.MainMenu
{
    public class MainMenuServices
    {
        readonly PlayerService playerService;
        readonly CharacterService characterService;
        readonly PlayFabService playFabService;
        readonly LeaderboardService leaderboardService;

        public PlayerService Player => playerService;
        public CharacterService Character => characterService;
        public PlayFabService PlayFabService => playFabService;
        public LeaderboardService LeaderboardService => leaderboardService;

        public MainMenuServices(
            PlayerService playerService,
            CharacterService characterService,
            PlayFabService playFabService,
            LeaderboardService leaderboardService)
        {
            this.playerService = playerService;
            this.characterService = characterService;
            this.playFabService = playFabService;
            this.leaderboardService = leaderboardService;
        }
    }
}