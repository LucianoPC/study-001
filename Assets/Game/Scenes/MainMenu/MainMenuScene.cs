using Game.Repositories;
using Game.Tasks;
using Game.UI.Screens;
using UGameArch.Core;
using UGameArch.Feature.Task;
using UGameArch.Providers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scenes.MainMenu
{
    public class MainMenuScene : BaseScene
    {
        [SerializeField] Camera camera;
        
        MainMenuServices services;
        ModelBoxRepository modelBoxes;
        SpriteProvider spriteProvider;
        ICoroutineHelper coroutineHelper;

        public void Initialize(
            bool isFirstInitialization,
            ModelBoxRepository modelBoxes,
            MainMenuServices services,
            SpriteProvider spriteProvider,
            ICoroutineHelper coroutineHelper)
        {
            this.services = services;
            this.modelBoxes = modelBoxes;
            this.spriteProvider = spriteProvider;
            this.coroutineHelper = coroutineHelper;
            
            ShowHomeScreen();

            if (isFirstInitialization)
            {
                ShowLoadingScreen();
            }
            else
            {
                GetLeaderboard();
            }
        }

        void ShowHomeScreen()
        {
            var screen = CreateScreen<HomeScreen>();
            screen.Setup(camera, modelBoxes, services.Player, services.Character, spriteProvider, coroutineHelper);
            screen.PlayPressed += () =>
            {
                LoadGameplayScene();
            };
            screen.LeaderboardPressed += () =>
            {
                ShowLeaderboardScreen();
            };
            screen.Initialize();
            SetCurrentScreen(screen);
        }

        void ShowLeaderboardScreen()
        {
            var screen = CreateScreen<LeaderboardScreen>();
            screen.Setup(modelBoxes, spriteProvider, coroutineHelper);
            screen.Back += () =>
            {
                ShowHomeScreen();
            };
            screen.Initialize();
            SetCurrentScreen(screen);
        }

        void ShowLoadingScreen()
        {
            var tasks = new ITask[]
            {
                new PlayFabLoginTask(services.PlayFabService),
                new GetLeaderboardTask(services.LeaderboardService),
            };
            
            var screen = CreateScreen<TaskScreen>();
            screen.Setup(tasks, coroutineHelper);
            screen.Finished += () =>
            {
                RemoveScreen(screen);
            };
            screen.Initialize();
            AddScreen(screen);
        }
        
        void LoadGameplayScene()
        {
            SceneManager.LoadScene(Constants.SceneName.Gameplay);
        }

        void GetLeaderboard()
        {
            var task = new GetLeaderboardTask(services.LeaderboardService);
            task.Run();
        }
    }
}