using Game.Models.Boxes;

namespace Game.RewardClaimers
{
    public class CurrencyRewardClaimer
    {
        const string SoftCurrency = Constants.Code.Currency.Soft;
        const string HardCurrency = Constants.Code.Currency.Hard;
        
        readonly WalletInfoBox walletInfoBox;

        public CurrencyRewardClaimer(WalletInfoBox walletInfoBox)
        {
            this.walletInfoBox = walletInfoBox;
        }

        public bool IsOwner(string rewardCode)
        {
            return IsCurrency(rewardCode);
        }

        public void ClaimReward(string code, long amount)
        {
            walletInfoBox.IncrementCurrency(code, amount);
        }

        bool IsCurrency(string code)
        {
            return IsSoftCurrency(code) || IsHardCurrency(code);
        }
        
        bool IsSoftCurrency(string code)
        {
            return code == SoftCurrency;
        }
        
        bool IsHardCurrency(string code)
        {
            return code == HardCurrency;
        }
    }
}