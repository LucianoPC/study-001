using Game.Models.Boxes;

namespace Game.RewardClaimers
{
    public class RewardClaimer
    {
        readonly CurrencyRewardClaimer currencyRewardClaimer;

        public RewardClaimer(WalletInfoBox walletInfoBox)
        {
            currencyRewardClaimer = new CurrencyRewardClaimer(walletInfoBox);
        }

        public void ClaimReward(string code, long amount)
        {
            if (currencyRewardClaimer.IsOwner(code))
            {
                currencyRewardClaimer.ClaimReward(code, amount);
            }
        }
    }
}