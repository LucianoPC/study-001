using System;

namespace Game.Helpers
{
    public static class DateTimeHelper
    {
        static readonly DateTime BeginDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
        
        public static DateTime FromUnix(long unix)
        {
            var dateTime = BeginDateTime.AddSeconds(unix);

            return dateTime;
        }

        public static long ToUnix(DateTime dateTime)
        {
            var unixTimestamp = dateTime.Subtract(BeginDateTime).TotalSeconds;

            return (long)unixTimestamp;
        }
    }
}