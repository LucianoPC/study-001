using System;
using Game.Helpers;
using Game.Models;
using Game.Models.Boxes;
using UGameArch.Core;

namespace Game.Services
{
    public class CharacterService
    {
        readonly WalletInfoBox walletInfoBox;
        readonly CharacterInfoBox characterInfoBox;
        readonly CharacterUpgradeInfoBox characterUpgradeInfoBox;

        public CharacterService(
            WalletInfoBox walletInfoBox,
            CharacterInfoBox characterInfoBox,
            CharacterUpgradeInfoBox characterUpgradeInfoBox)
        {
            this.walletInfoBox = walletInfoBox;
            this.characterInfoBox = characterInfoBox;
            this.characterUpgradeInfoBox = characterUpgradeInfoBox;
        }

        public void LevelUp(string characterId, Action onSuccess, Action<IError> onError)
        {
            var characterInfo = characterInfoBox.GetCharacterInfo(characterId);
            var upgradeIndex = characterInfo.level - 1;
            var upgrades = characterUpgradeInfoBox.GetCharacterUpgradeInfo(characterId).upgradeItems;
            var hasNextUpgrade = upgradeIndex < upgrades.Count - 1;

            if (!hasNextUpgrade)
            {
                onError?.Invoke(new Error(Constants.Errors.Code.NotHasNextUpgrade, Constants.Errors.Message.NotHasNextUpgrade));
                return;
            }

            var nextUpgrade = upgrades[upgradeIndex + 1];

            if (characterInfo.isUpgrading)
            {
                onError?.Invoke(new Error(Constants.Errors.Code.WaitUpgrade, Constants.Errors.Message.WaitUpgrade));
                return;
            }

            var hasEnoughCurrency = walletInfoBox.HasEnoughCurrency(nextUpgrade.levelUpCurrencyCode, nextUpgrade.levelUpCost);

            if (!hasEnoughCurrency)
            {
                onError?.Invoke(new Error(Constants.Errors.Code.NotEnoughCurrency, Constants.Errors.Message.NotEnoughCurrency));
                return;
            }

            var hasSpeedUpCost = nextUpgrade.speedUpCost > 0;

            if (hasSpeedUpCost)
            {
                characterInfo.isUpgrading = true;
                characterInfo.unixUpgradedAt = DateTimeHelper.ToUnix(DateTime.UtcNow) + (long)nextUpgrade.speedUpSeconds;
            }
            else
            {
                characterInfo.level += 1;
            }
            
            characterInfoBox.Update();
            characterUpgradeInfoBox.Update();
            
            walletInfoBox.DecrementCurrency(nextUpgrade.levelUpCurrencyCode, nextUpgrade.levelUpCost);
            
            onSuccess?.Invoke();
        }

        public void SpeedUp(string characterId, Action onSuccess, Action<IError> onError)
        {
            var characterInfo = characterInfoBox.GetCharacterInfo(characterId);
            var upgradeIndex = characterInfo.level - 1;
            var upgrades = characterUpgradeInfoBox.GetCharacterUpgradeInfo(characterId).upgradeItems;
            var hasNextUpgrade = upgradeIndex < upgrades.Count - 1;

            if (!hasNextUpgrade)
            {
                onError?.Invoke(new Error(Constants.Errors.Code.NotHasNextUpgrade, Constants.Errors.Message.NotHasNextUpgrade));
                return;
            }

            var nextUpgrade = upgrades[upgradeIndex + 1];

            if (!characterInfo.isUpgrading)
            {
                onError?.Invoke(new Error(Constants.Errors.Code.NotUpgrading, Constants.Errors.Message.NotUpgrading));
                return;
            }
            
            var timeNow = DateTime.UtcNow;
            var upgradedAt = DateTimeHelper.FromUnix(characterInfo.unixUpgradedAt);
            var hasUpgradedAtFinished = timeNow >= upgradedAt;

            if (!hasUpgradedAtFinished)
            {
                var hasEnoughCurrency = walletInfoBox.HasEnoughCurrency(nextUpgrade.speedUpCurrencyCode, nextUpgrade.speedUpCost);

                if (!hasEnoughCurrency)
                {
                    onError?.Invoke(new Error(Constants.Errors.Code.NotEnoughCurrency, Constants.Errors.Message.NotEnoughCurrency));
                    return;
                }
                
                walletInfoBox.DecrementCurrency(nextUpgrade.speedUpCurrencyCode, nextUpgrade.speedUpCost);
            }

            characterInfo.isUpgrading = false;
            characterInfo.unixUpgradedAt = 0;
            characterInfo.level += 1;
            
            characterInfoBox.Update();
            characterUpgradeInfoBox.Update();
            
            onSuccess?.Invoke();
        }
    }
}