using System;
using System.Collections.Generic;
using Game.Models;
using Game.Models.Boxes;
using PlayFab;
using PlayFab.ClientModels;
using UGameArch.Core;

namespace Game.Services
{
    public class PlayFabService
    {
        readonly PlayerInfoBox playerInfoBox;
        readonly PlayFabAccountInfoBox playFabAccountInfoBox;

        public PlayFabService(PlayerInfoBox playerInfoBox, PlayFabAccountInfoBox playFabAccountInfoBox)
        {
            this.playerInfoBox = playerInfoBox;
            this.playFabAccountInfoBox = playFabAccountInfoBox;
        }

        public void Login(Action onSuccess, Action<IError> onError)
        {
            if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId)){
                PlayFabSettings.staticSettings.TitleId = "Study-001";
            }

            var request = new LoginWithCustomIDRequest
            {
                CustomId = playerInfoBox.Model.userId,
                CreateAccount = true
            };
            
            PlayFabClientAPI.LoginWithCustomID(request, 
                resultCallback: result =>
                {
                    playFabAccountInfoBox.SetPlayFabId(result.PlayFabId);
                    SetName(playerInfoBox.Model.name, onSuccess, onError);
                },
                errorCallback: error =>
                {
                    onError?.Invoke(new Error($"PlayFab-{error.HttpCode}", error.ErrorMessage));
                });
        }

        public void SetName(string name, Action onSuccess, Action<IError> onError)
        {
            var request = new UpdateUserTitleDisplayNameRequest
            {
                DisplayName = name,
            };
            
            PlayFabClientAPI.UpdateUserTitleDisplayName(request,
                resultCallback: result =>
                {
                    onSuccess?.Invoke();
                },
                errorCallback: error =>
                {
                    onError?.Invoke(new Error($"PlayFab-{error.HttpCode}", error.ErrorMessage));
                });
        }

        public void SetBestTime(int bestTimeMilliseconds, Action onSuccess, Action<IError> onError)
        {
            var request = new UpdatePlayerStatisticsRequest
            {
                Statistics = new List<StatisticUpdate>
                {
                    new StatisticUpdate
                    {
                        Value = bestTimeMilliseconds * -1,
                        StatisticName = Constants.Leaderboard.BestTime,
                    },
                },
            };
            
            PlayFabClientAPI.UpdatePlayerStatistics(request,
                resultCallback: result =>
                {
                    onSuccess?.Invoke();
                },
                errorCallback: error =>
                {
                    onError?.Invoke(new Error($"PlayFab-{error.HttpCode}", error.ErrorMessage));
                });
        }

        public void GetLeaderboard(Action<LeaderboardInfo> onSuccess, Action<IError> onError)
        {
            var request = new GetLeaderboardRequest
            {
                StatisticName = Constants.Leaderboard.BestTime,
                MaxResultsCount = 20,
            };
            
            PlayFabClientAPI.GetLeaderboard(request,
                resultCallback: result =>
                {
                    var leaderboardInfo = new LeaderboardInfo
                    {
                        items = new LeaderboardItemInfo[result.Leaderboard.Count]
                    };

                    for (var i = 0; i < result.Leaderboard.Count; i++)
                    {
                        leaderboardInfo.items[i] = GetLeaderboardItemInfo(result.Leaderboard[i]);
                    }
                    
                    onSuccess?.Invoke(leaderboardInfo);
                },
                errorCallback: error =>
                {
                    onError?.Invoke(new Error($"PlayFab-{error.HttpCode}", error.ErrorMessage));
                });
        }

        LeaderboardItemInfo GetLeaderboardItemInfo(PlayerLeaderboardEntry entry)
        {
            return new LeaderboardItemInfo
            {
                isCurrentPlayer = entry.PlayFabId == playFabAccountInfoBox.Model.playFabId,
                playerName = entry.DisplayName,
                bestTimeSeconds = (entry.StatValue * -1) / 1000.0,
            };
        }
    }
}