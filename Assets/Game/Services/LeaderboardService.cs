using System;
using Game.Models.Boxes;
using UGameArch.Core;

namespace Game.Services
{
    public class LeaderboardService
    {
        readonly LeaderboardInfoBox leaderboardInfoBox;
        readonly PlayFabService playFabService;

        public LeaderboardService(LeaderboardInfoBox leaderboardInfoBox, PlayFabService playFabService)
        {
            this.leaderboardInfoBox = leaderboardInfoBox;
            this.playFabService = playFabService;
        }

        public void GetLeaderboard(Action onSuccess, Action<IError> onError)
        {
            playFabService.GetLeaderboard(
                onSuccess: leaderboardInfo =>
                {
                    leaderboardInfoBox.Set(leaderboardInfo);
                    onSuccess?.Invoke();
                },
                onError: onError);
        }
    }
}