using System;
using Game.Models.Boxes;
using UGameArch.Core;
using UnityEngine;

namespace Game.Services
{
    public class PlayerService
    {
        readonly PlayerInfoBox playerInfoBox;
        readonly PlayFabService playFabService;

        public PlayerService(PlayerInfoBox playerInfoBox, PlayFabService playFabService)
        {
            this.playerInfoBox = playerInfoBox;
            this.playFabService = playFabService;
        }

        public void SetName(string name, Action onSuccess, Action<IError> onError)
        {
            playFabService.SetName(name,
                onSuccess: () =>
                {
                    playerInfoBox.SetName(name);
                },
                onError: onError);
            
            onSuccess?.Invoke();
        }

        public void SetBestTime(double bestTimeSeconds, Action onSuccess, Action<IError> onError)
        {
            var isBestTime = bestTimeSeconds < playerInfoBox.Model.bestTimeSeconds;
            if (!isBestTime)
            {
                onSuccess?.Invoke();
                return;
            }

            var bestTimeMilliseconds = (int)(bestTimeSeconds * 1000);
            
            playFabService.SetBestTime(bestTimeMilliseconds,
                onSuccess: () =>
                {
                    playerInfoBox.SetBestTimeSeconds(bestTimeSeconds);
                    onSuccess?.Invoke();
                },
                onError: error =>
                {
                    Debug.Log($"[ERROR] SetBestTime [code: {error.Code}] [message: {error.Message}]");
                    
                    playerInfoBox.SetBestTimeSeconds(bestTimeSeconds);
                    onSuccess?.Invoke();
                });
        }
    }
}