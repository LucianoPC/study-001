using System;
using Game.Services;
using UGameArch.Core;

namespace Game.Tasks
{
    public class GetLeaderboardTask : ITask
    {
        const string TaskName = "Loading leaderboard";

        readonly LeaderboardService leaderboardService;

        public string Name => TaskName;
        
        public event Action Finished;
        public event Action<IError> Failed;

        public GetLeaderboardTask(LeaderboardService leaderboardService)
        {
            this.leaderboardService = leaderboardService;
        }

        public void Run()
        {
            leaderboardService.GetLeaderboard(
                onSuccess: Finished,
                onError: Failed);
        }
    }
}