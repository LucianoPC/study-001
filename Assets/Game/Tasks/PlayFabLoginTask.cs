using System;
using Game.Services;
using UGameArch.Core;

namespace Game.Tasks
{
    public class PlayFabLoginTask : ITask
    {
        const string TaskName = "PlayFab Login";

        readonly PlayFabService playFabService;

        public string Name => TaskName;
        
        public event Action Finished;
        public event Action<IError> Failed;

        public PlayFabLoginTask(PlayFabService playFabService)
        {
            this.playFabService = playFabService;
        }

        public void Run()
        {
            playFabService.Login(
                onSuccess: Finished,
                onError: Failed);
        }
    }
}