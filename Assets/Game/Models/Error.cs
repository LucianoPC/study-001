using UGameArch.Core;

namespace Game.Models
{
    public class Error : IError
    {
        public string Code { get; }
        public string Message { get; }

        public Error(string code, string message)
        {
            Code = code;
            Message = message;
        }
    }
}