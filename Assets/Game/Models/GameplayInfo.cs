using System;

namespace Game.Models
{
    [Serializable]
    public class GameplayInfo
    {
        public RewardInfo[] winRewards;
        public RewardInfo[] loseRewards;
    }
}