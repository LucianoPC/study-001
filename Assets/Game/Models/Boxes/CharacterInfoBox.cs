using System.Collections.Generic;
using System.Linq;
using UGameArch.Core;

namespace Game.Models.Boxes
{
    public class CharacterInfoBox : ModelBox<List<CharacterInfo>>
    {
        public CharacterInfoBox()
        {
        }

        public CharacterInfoBox(List<CharacterInfo> model) : base(model)
        {
        }

        public CharacterInfo GetCharacterInfo(string characterId)
        {
            return Model.FirstOrDefault(characterInfo => characterInfo.id == characterId);
        }
    }
}