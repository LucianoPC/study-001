using UGameArch.Core;

namespace Game.Models.Boxes
{
    public class PlayerInfoBox : ModelBox<PlayerInfo>
    {
        public PlayerInfoBox()
        {
        }

        public PlayerInfoBox(PlayerInfo model) : base(model)
        {
        }

        public void SetName(string name)
        {
            Model.name = name;
            NotifyUpdated();
        }

        public void SetBestTimeSeconds(double bestTimeSeconds)
        {
            Model.bestTimeSeconds = bestTimeSeconds;
            NotifyUpdated();
        }
    }
}