using System.Collections.Generic;
using System.Linq;
using UGameArch.Core;

namespace Game.Models.Boxes
{
    public class CharacterUpgradeInfoBox : ModelBox<List<CharacterUpgradeInfo>>
    {
        public CharacterUpgradeInfoBox()
        {
        }

        public CharacterUpgradeInfoBox(List<CharacterUpgradeInfo> model) : base(model)
        {
        }

        public CharacterUpgradeInfo GetCharacterUpgradeInfo(string characterId)
        {
            return Model.FirstOrDefault(characterUpgradeInfo => characterUpgradeInfo.characterId == characterId);
        }
    }
}