using UGameArch.Core;

namespace Game.Models.Boxes
{
    public class PlayFabAccountInfoBox : ModelBox<PlayFabAccountInfo>
    {
        public void SetPlayFabId(string id)
        {
            Model.playFabId = id;
            NotifyUpdated();
        }
    }
}