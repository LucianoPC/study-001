using UGameArch.Core;

namespace Game.Models.Boxes
{
    public class WalletInfoBox : ModelBox<WalletInfo>
    {
        public WalletInfoBox()
        {
        }

        public WalletInfoBox(WalletInfo model) : base(model)
        {
        }

        public long GetCurrency(string currencyCode)
        {
            return Model.GetCurrency(currencyCode);
        }

        public void SetCurrency(string currencyCode, long amount)
        {
            Model.SetCurrency(currencyCode, amount);
            NotifyUpdated();
        }

        public void IncrementCurrency(string currencyCode, long amount)
        {
            Model.IncrementCurrency(currencyCode, amount);
            NotifyUpdated();
        }
        
        public void DecrementCurrency(string currencyCode, long amount)
        {
            Model.DecrementCurrency(currencyCode, amount);
            NotifyUpdated();
        }

        public bool HasEnoughCurrency(string currencyCode, long amount)
        {
            var currency = GetCurrency(currencyCode);
            return currency >= amount;
        }
    }
}