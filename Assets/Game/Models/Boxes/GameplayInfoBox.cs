using UGameArch.Core;

namespace Game.Models.Boxes
{
    public class GameplayInfoBox : ModelBox<GameplayInfo>
    {
        public GameplayInfoBox()
        {
        }

        public GameplayInfoBox(GameplayInfo model) : base(model)
        {
        }
    }
}