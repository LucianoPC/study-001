using System;

namespace Game.Models
{
    [Serializable]
    public class CharacterUpgradeItemInfo
    {
        public float speed;
        public long levelUpCost;
        public string levelUpCurrencyCode;
        
        public long speedUpCost;
        public string speedUpCurrencyCode;
        public float speedUpSeconds;
    }
}