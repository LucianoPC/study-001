using System;

namespace Game.Models
{
    [Serializable]
    public class RewardInfo
    {
        public string code;
        public long amount;
    }
}