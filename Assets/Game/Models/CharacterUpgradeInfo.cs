using System.Collections.Generic;

namespace Game.Models
{
    public class CharacterUpgradeInfo
    {
        public string characterId;
        public List<CharacterUpgradeItemInfo> upgradeItems;
    }
}