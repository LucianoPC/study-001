using System;

namespace Game.Models
{
    [Serializable]
    public class LeaderboardInfo
    {
        public LeaderboardItemInfo[] items;
    }
}