using System;

namespace Game.Models
{
    [Serializable]
    public class PlayerInfo
    {
        public string userId;
        public string name;
        public string selectedCharacterId;
        public double bestTimeSeconds;
    }
}