using System;

namespace Game.Models
{
    [Serializable]
    public class CharacterInfo
    {
        public string id;
        public int level;
        
        public bool isUpgrading;
        public long unixUpgradedAt;
    }
}