using System;
using System.Collections.Generic;

namespace Game.Models
{
    [Serializable]
    public class WalletInfo
    {
        public Dictionary<string, long> currencyMap;

        public long GetCurrency(string currencyCode)
        {
            return currencyMap[currencyCode];
        }

        public void SetCurrency(string currencyCode, long amount)
        {
            currencyMap[currencyCode] = amount;
        }

        public void IncrementCurrency(string currencyCode, long amount)
        {
            currencyMap[currencyCode] += amount;
        }
        
        public void DecrementCurrency(string currencyCode, long amount)
        {
            currencyMap[currencyCode] -= amount;
        }
    }
}