using System;

namespace Game.Models
{
    [Serializable]
    public class LeaderboardItemInfo
    {
        public string playerName;
        public double bestTimeSeconds;
        public bool isCurrentPlayer;
    }
}