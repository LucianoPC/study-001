using System;

namespace Game.Gameplay.UI.Interfaces
{
    public interface ITimer
    {
        double TotalSeconds { get; }
        TimeSpan CurrentTimeSpan { get; }
    }
}