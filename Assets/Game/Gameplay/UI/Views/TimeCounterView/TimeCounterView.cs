using TMPro;
using UnityEngine;

namespace Game.UI.Views
{
    public class TimeCounterView : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI timerText;


        public void SetTimerText(string text)
        {
            timerText.text = text;
        }
    }
}