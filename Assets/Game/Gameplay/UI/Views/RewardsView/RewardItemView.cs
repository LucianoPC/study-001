using TMPro;
using UGameArch.UI;
using UnityEngine;

namespace Game.UI.Views
{
    public class RewardItemView : MonoBehaviour
    {
        [SerializeField] IconView iconView;
        [SerializeField] TextMeshProUGUI amountText;

        public void SetIcon(Sprite icon)
        {
            iconView.SetIcon(icon);
        }

        public void SetAmount(string amount)
        {
            amountText.text = amount;
        }
    }
}