using UnityEngine;

namespace Game.UI.Views
{
    public class RewardsView : MonoBehaviour
    {
        [Header("Reward Item View")]
        [SerializeField] GameObject rewardItemViewPrefab;
        [SerializeField] Transform rewardItemViewParent;

        public void SetRewardItems(RewardItem[] rewardItems)
        {
            var children = rewardItemViewParent.GetComponentsInChildren<RewardItemView>();
            foreach (var child in children) { Destroy(child.gameObject); }

            foreach (var rewardItem in rewardItems)
            {
                var rewardItemView = Instantiate(rewardItemViewPrefab, rewardItemViewParent).GetComponent<RewardItemView>();
                
                rewardItem.SetRewardItemView(rewardItemView);
            }
        }

        public class RewardItem
        {
            public Sprite Icon;
            public string Amount;

            public void SetRewardItemView(RewardItemView view)
            {
                view.SetIcon(Icon);
                view.SetAmount(Amount);
            }
        }
    }
}