using TMPro;
using UnityEngine;

namespace Game.UI.Views
{
    public class ResultTimeView : MonoBehaviour
    {
        [SerializeField] GameObject newHighScoreContainer;
        [SerializeField] GameObject bestTimeContainer;
        [SerializeField] TextMeshProUGUI timeText;
        [SerializeField] TextMeshProUGUI bestTimeText;

        public void SetNewHighScoreVisible(bool isVisible)
        {
            newHighScoreContainer.SetActive(isVisible);
        }

        public void SetTime(string text)
        {
            timeText.text = text;
        }

        public void SetBestTime(string text)
        {
            bestTimeText.text = text;
        }

        public void SetBestTimeVisible(bool isVisible)
        {
            bestTimeContainer.SetActive(isVisible);
        }
    }
}