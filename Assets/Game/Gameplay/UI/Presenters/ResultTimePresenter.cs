using System;
using Game.UI.Views;

namespace Game.Gameplay.UI.Presenters
{
    public class ResultTimePresenter
    {
        readonly bool isNewHighScore;
        readonly double timeSeconds;
        readonly double bestTimeSeconds;
        readonly ResultTimeView resultTimeView;

        public ResultTimePresenter(
            bool isNewHighScore,
            double timeSeconds,
            double bestTimeSeconds,
            ResultTimeView resultTimeView)
        {
            this.isNewHighScore = isNewHighScore;
            this.timeSeconds = timeSeconds;
            this.bestTimeSeconds = bestTimeSeconds;
            this.resultTimeView = resultTimeView;
        }

        public void Initialize()
        {
            UpdateResultTimeView();
        }

        public void Dispose()
        {
        }

        void UpdateResultTimeView()
        {
            var timeText = GetTimeText(timeSeconds);
            var isBestTimeVisible = bestTimeSeconds < double.MaxValue;
            var bestTimeText = isBestTimeVisible ? GetTimeText(bestTimeSeconds) : "-";
            
            resultTimeView.SetTime(timeText);
            resultTimeView.SetBestTime(bestTimeText);
            resultTimeView.SetNewHighScoreVisible(isNewHighScore);
            resultTimeView.SetBestTimeVisible(isBestTimeVisible);
        }

        string GetTimeText(double timeSeconds)
        {
            var timeSpan = TimeSpan.FromSeconds(timeSeconds);
            return $"{timeSpan.Minutes:D2}:{timeSpan.Seconds:D2}:{timeSpan.Milliseconds:D3}";
        }
    }
}