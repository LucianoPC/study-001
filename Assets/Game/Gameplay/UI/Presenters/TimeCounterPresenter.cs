using System.Collections;
using Game.Gameplay.UI.Interfaces;
using TMPro;
using UGameArch.Core;
using UnityEngine;

namespace Game.Gameplay.UI.Presenters
{
    public class TimeCounterPresenter
    {
        readonly ITimer timer;
        readonly TextMeshProUGUI textMeshProUGUI;
        readonly ICoroutineHelper coroutineHelper;

        Coroutine updateTimerTextCoroutine;

        public TimeCounterPresenter(ITimer timer, TextMeshProUGUI textMeshProUGUI, ICoroutineHelper coroutineHelper)
        {
            this.timer = timer;
            this.textMeshProUGUI = textMeshProUGUI;
            this.coroutineHelper = coroutineHelper;
        }

        public void Initialize()
        {
            updateTimerTextCoroutine = coroutineHelper.StartCoroutine(UpdateTimerText());
        }

        public void Dispose()
        {
            if (updateTimerTextCoroutine != null) { coroutineHelper.StopCoroutine(updateTimerTextCoroutine); }
        }

        IEnumerator UpdateTimerText()
        {
            while (true)
            {
                var deltaTime = timer.CurrentTimeSpan;
                var deltaTimeText = $"{deltaTime.Minutes:D2}:{deltaTime.Seconds:D2}:{deltaTime.Milliseconds:D3}";

                textMeshProUGUI.text = deltaTimeText;

                yield return null;
            }
        }
    }
}