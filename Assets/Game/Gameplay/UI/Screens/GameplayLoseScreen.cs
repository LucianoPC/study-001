using System;
using Game.Gameplay.UI.Presenters;
using Game.Models;
using Game.Models.Boxes;
using Game.Presenters;
using Game.UI.Views;
using Game.UI.Views.TopBar;
using UGameArch.Core;
using UGameArch.Helpers;
using UGameArch.Providers;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Gameplay.UI.Screens
{
    public class GameplayLoseScreen : BaseScreen
    {
        [SerializeField] ResultTimeView resultTimeView;
        [SerializeField] RewardsView rewardsView;
        [SerializeField] TopBarCurrenciesView topBarView;
        [SerializeField] Button quitButton;
        [SerializeField] Button playAgainButton;
        [SerializeField] CanvasGroup canvasGroup;
        
        RewardsPresenter rewardsPresenter;
        ResultTimePresenter resultTimePresenter;
        TopBarCurrenciesPresenter topBarPresenter;
        ICoroutineHelper coroutineHelper;

        public event Action QuitPressed;
        public event Action PlayAgainPressed;
        
        public void Setup(
            double timeSeconds,
            double bestTimeSeconds,
            WalletInfoBox walletInfoBox,
            SpriteProvider spriteProvider,
            RewardInfo[] rewardInfos,
            ICoroutineHelper coroutineHelper)
        {
            this.coroutineHelper = coroutineHelper;
            
            resultTimePresenter =
                new ResultTimePresenter(isNewHighScore: false, timeSeconds, bestTimeSeconds, resultTimeView);
            
            rewardsPresenter = new RewardsPresenter(rewardsView, spriteProvider, rewardInfos);
            
            topBarPresenter = new TopBarCurrenciesPresenter(walletInfoBox, topBarView, spriteProvider, coroutineHelper);
        }
        
        public override void Initialize()
        {
            quitButton.onClick.AddListener(NotifyQuitPressed);
            playAgainButton.onClick.AddListener(NotifyPlayAgainPressed);
            
            resultTimePresenter.Initialize();
            rewardsPresenter.Initialize();
            topBarPresenter.Initialize();
            coroutineHelper.StartCoroutine(CanvasGroupHelper.CoroutineFadeIn(canvasGroup));
        }

        void OnDestroy()
        {
            quitButton.onClick.RemoveListener(NotifyQuitPressed);
            playAgainButton.onClick.RemoveListener(NotifyPlayAgainPressed);
            
            resultTimePresenter?.Dispose();
            rewardsPresenter?.Dispose();
            topBarPresenter?.Dispose();
        }
        
        void NotifyQuitPressed()
        {
            QuitPressed?.Invoke();
        }

        void NotifyPlayAgainPressed()
        {
            PlayAgainPressed?.Invoke();
        }
    }
}