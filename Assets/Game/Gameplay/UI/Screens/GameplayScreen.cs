using Game.Gameplay.UI.Interfaces;
using Game.Gameplay.UI.Presenters;
using TMPro;
using UGameArch.Core;
using UnityEngine;

namespace Game.Gameplay.UI.Screens
{
    public class GameplayScreen : BaseScreen
    {
        [SerializeField] TextMeshProUGUI timerText;
        [SerializeField] GameObject tapToPlayContainer;
        [SerializeField] GameObject tapToJumpContainer;

        GameStateMachine gameStateMachine;
        TimeCounterPresenter timeCounterPresenter;

        public void Setup(
            GameStateMachine gameStateMachine,
            ITimer gameplayTimer,
            ICoroutineHelper coroutineHelper)
        {
            this.gameStateMachine = gameStateMachine;
            
            timeCounterPresenter = new TimeCounterPresenter(gameplayTimer, timerText, coroutineHelper);
        }

        public override void Initialize()
        {
            gameStateMachine.StateChanged += OnGameStateChanged;
            
            timeCounterPresenter.Initialize();
            
            SetTapToPlayVisible(true);
            SetTapToJumpVisible(false);
        }

        void OnDestroy()
        {
            gameStateMachine.StateChanged -= OnGameStateChanged;
            
            timeCounterPresenter?.Dispose();
        }

        void OnGameStateChanged(GameStateMachine.State state)
        {
            if (state == GameStateMachine.State.Started)
            {
                SetTapToPlayVisible(false);
                SetTapToJumpVisible(true);
            }
        }

        void SetTapToPlayVisible(bool isVisible)
        {
            tapToPlayContainer.SetActive(isVisible);
        }

        void SetTapToJumpVisible(bool isVisible)
        {
            tapToJumpContainer.SetActive(isVisible);
        }
    }
}