﻿using UnityEngine;

namespace Game.UI.Utils
{
    public class FixSafeAreaResolution : MonoBehaviour
    {
        RectTransform rectTransform;

        void Awake()
        {
            rectTransform = GetComponent<RectTransform>();

            Apply();
        }

        [ContextMenu("Apply")]
        void Apply()
        {
            var screenWidth = Screen.width;
            var safeAreaWidth = Screen.safeArea.width;
            var safeAreaDistance = (screenWidth - safeAreaWidth) / 1f;

            var leftDistance = safeAreaDistance * -1;
            var rightDistance = safeAreaDistance + rectTransform.offsetMin.x;
            rectTransform.offsetMax = new Vector2(leftDistance, rectTransform.offsetMax.y);
            rectTransform.offsetMin = new Vector2(rightDistance, rectTransform.offsetMin.y);
        }
    }
}
