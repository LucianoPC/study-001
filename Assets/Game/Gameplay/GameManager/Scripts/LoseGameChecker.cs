using System;
using UnityEngine;

namespace Game.Gameplay
{
    public class LoseGameChecker : MonoBehaviour
    {
        [SerializeField] float yPositionToLose = -8f;
        [SerializeField] GamePlayerModel playerModel;

        public event Action Lost;

        void Update()
        {
            UpdateCheckLoseGame();
        }

        public void SetEnabled(bool enabled)
        {
            this.enabled = enabled;
        }

        void UpdateCheckLoseGame()
        {
            var hasLost = playerModel.transform.localPosition.y < yPositionToLose;

            if (hasLost)
            {
                Lost?.Invoke();
            }
        }
    }
}