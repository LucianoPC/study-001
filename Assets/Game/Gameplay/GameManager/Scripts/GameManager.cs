using System;
using UnityEngine;

namespace Game.Gameplay
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] GameStarter gameStarter;
        [SerializeField] WinGameChecker winGameChecker;
        [SerializeField] LoseGameChecker loseGameChecker;
        [SerializeField] GamePlayerController playerController;
        [SerializeField] GamePlayerModel gamePlayerModel;

        double bestTime;
        GameTimer gameTimer;
        GameStateMachine gameStateMachine;

        public GameTimer GameTimer => gameTimer;
        public GameStateMachine GameStateMachine => gameStateMachine;

        public event Action<GameResult> WonGame;
        public event Action<GameResult> LostGame;

        void Awake()
        {
            gameTimer = new GameTimer();
            gameStateMachine = new GameStateMachine();
            
            gameStarter.SetEnabled(false);
            loseGameChecker.SetEnabled(false);
            playerController.SetEnabled(false);
        }

        public void Initialize(double bestTime, GamePlayerAttributes attributes)
        {
            this.bestTime = bestTime;
            
            gamePlayerModel.SetAttributes(attributes);
            
            gameTimer.Reset();
            gameStarter.SetEnabled(true);
            gameStarter.Started += OnGameStarted;
        }

        void OnGameStarted()
        {
            gameStateMachine.SetState(GameStateMachine.State.Started);
            
            gameStarter.Started -= OnGameStarted;
            
            loseGameChecker.SetEnabled(true);
            playerController.SetEnabled(true);
            
            gameTimer.StartTimer();

            winGameChecker.Won += OnWonGame;
            loseGameChecker.Lost += OnLostGame;
        }

        void OnWonGame()
        {
            StopGame();

            var result = GetGameResult(hasWon: true);
            
            WonGame?.Invoke(result);
        }

        void OnLostGame()
        {
            StopGame();
            
            var result = GetGameResult(hasWon: false);
            
            LostGame?.Invoke(result);
        }

        void StopGame()
        {
            gameStateMachine.SetState(GameStateMachine.State.Finished);
            
            winGameChecker.Won -= OnWonGame;
            loseGameChecker.Lost -= OnLostGame;
            
            loseGameChecker.SetEnabled(false);
            playerController.SetEnabled(false);
            
            gameTimer.StopTimer();
        }

        GameResult GetGameResult(bool hasWon)
        {
            var isNewHighScore = hasWon && gameTimer.TotalSeconds < this.bestTime;
            var time = gameTimer.TotalSeconds;
            var bestTime = isNewHighScore ? time : this.bestTime;
            
            return new GameResult(hasWon, isNewHighScore, time, bestTime);
        }
    }
}