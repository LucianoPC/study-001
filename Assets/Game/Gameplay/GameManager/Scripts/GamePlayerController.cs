using UnityEngine;

namespace Game.Gameplay
{
    public class GamePlayerController : MonoBehaviour
    {
        [SerializeField] GamePlayerModel playerModel;

        void Update()
        {
            UpdateInput();
        }
        
        public void SetEnabled(bool enabled)
        {
            this.enabled = enabled;

            if (enabled)
            {
                SetEnabled();
            }
            else
            {
                SetDisabled();
            }
        }

        void SetEnabled()
        {
            playerModel.EnableMovement(true);
            playerModel.Run();
        }

        void SetDisabled()
        {
            playerModel.StopRun();
            playerModel.EnableMovement(false);
        }

        void UpdateInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                playerModel.Jump();
            }            
        }
    }
}