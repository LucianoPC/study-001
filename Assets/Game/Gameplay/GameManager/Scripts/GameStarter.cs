using System;
using UnityEngine;

namespace Game.Gameplay
{
    public class GameStarter : MonoBehaviour
    {
        public event Action Started;

        void Update()
        {
            UpdateStartGame();
        }

        public void SetEnabled(bool enabled)
        {
            this.enabled = enabled;
        }

        void UpdateStartGame()
        {
            if(Input.GetMouseButtonDown(0))
            {
                StartGame();
            }
        }

        void StartGame()
        {
            Started?.Invoke();
            
            SetEnabled(false);
        }
    }
}