using System;
using UnityEngine;

namespace Game.Gameplay
{
    public class WinGameChecker : MonoBehaviour
    {
        [SerializeField] FinishLine finishLine;
        
        public event Action Won;

        void Awake()
        {
            finishLine.Collided += OnFinishLineCollided;
        }

        void OnFinishLineCollided()
        {
            finishLine.Collided -= OnFinishLineCollided;
            
            Won?.Invoke();
        }
    }
}