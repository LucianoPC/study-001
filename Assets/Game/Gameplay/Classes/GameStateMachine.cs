using System;

namespace Game.Gameplay
{
    public class GameStateMachine
    {
        public enum State
        {
            Waiting,
            Started,
            Finished,
        }

        State state;

        public State CurrentState => state;

        public event Action<State> StateChanged;

        public GameStateMachine()
        {
            state = State.Waiting;
        }

        public void SetState(State state)
        {
            var oldState = this.state;
            this.state = state;

            if (state != oldState)
            {
                StateChanged?.Invoke(state);
            }
        }
    }
}