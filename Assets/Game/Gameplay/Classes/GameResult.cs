namespace Game.Gameplay
{
    public class GameResult
    {
        readonly bool hasWon;
        readonly bool isNewHighScore;
        readonly double time;
        readonly double bestTime;

        public bool HasWon => hasWon;
        public bool IsNewHighScore => isNewHighScore;
        public double Time => time;
        public double BestTime => bestTime;

        public GameResult(bool hasWon, bool isNewHighScore, double time, double bestTime)
        {
            this.hasWon = hasWon;
            this.isNewHighScore = isNewHighScore;
            this.time = time;
            this.bestTime = bestTime;
        }
    }
}