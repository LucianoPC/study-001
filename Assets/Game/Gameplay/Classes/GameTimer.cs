using System;
using Game.Gameplay.UI.Interfaces;

namespace Game.Gameplay
{
    public class GameTimer : ITimer
    {
        DateTime startedTime;
        DateTime stoppedTime;

        bool hasStarted;
        bool hasStopped;

        public double TotalSeconds => CurrentTimeSpan.TotalSeconds;
        public TimeSpan CurrentTimeSpan => GetCurrentTime();

        public void Reset()
        {
            hasStarted = false;
            hasStopped = false;
        }

        public void StartTimer()
        {
            startedTime = DateTime.Now;

            hasStarted = true;
            hasStopped = false;
        }

        public void StopTimer()
        {
            stoppedTime = DateTime.Now;

            hasStarted = false;
            hasStopped = true;
        }

        TimeSpan GetCurrentTime()
        {
            if (hasStarted)
            {
                return DateTime.Now - startedTime;
            }
            else if(hasStopped)
            {
                return stoppedTime - startedTime;
            }
            else
            {
                return TimeSpan.Zero;
            }
        }
    }
}