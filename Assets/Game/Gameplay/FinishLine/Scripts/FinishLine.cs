using System;
using UnityEngine;

namespace Game.Gameplay
{
    public class FinishLine : MonoBehaviour
    {
        [SerializeField] Collider collider;
        
        public event Action Collided;

        void OnTriggerEnter(Collider other)
        {
            Collided?.Invoke();
        }
    }
}