using UnityEngine;

namespace Game.Gameplay
{
    public class GamePlayerModel : MonoBehaviour
    {
        [SerializeField] new Rigidbody rigidbody;
        [SerializeField] ForceMode jumpForceMode;
        [SerializeField] CharacterModel characterModel;
        [SerializeField] ConstantForce constantForce;
        [SerializeField] LayerMask groundLayerMask;
        [SerializeField] GamePlayerInfo gamePlayerInfo;

        bool isJumping;
        bool isRunning;
        bool isGrounded;

        int groundLayerMaskBytes;

        void Awake()
        {
            isJumping = false;
            isRunning = false;
            isGrounded = true;

            groundLayerMaskBytes = ~(1 << groundLayerMask.value);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                Jump();
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                Run();
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                StopRun();
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                EnableMovement(true);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                EnableMovement(false);
            }
            
            UpdateRunVelocity();
        }

        void LateUpdate()
        {
            UpdateIsGrounded();
        }

        void UpdateRunVelocity()
        {
            var velocity = rigidbody.velocity;
            velocity.z = isRunning ? gamePlayerInfo.runSpeed : 0f;
            rigidbody.velocity = velocity;
        }

        public void SetAttributes(GamePlayerAttributes attributes)
        {
            gamePlayerInfo.runSpeed = attributes.speed;
        }

        public void Run()
        {
            isRunning = true;
            
            UpdateRunVelocity();

            characterModel.SetState(CharacterModel.State.Running);
        }

        public void StopRun()
        {
            isRunning = false;
            
            UpdateRunVelocity();
            
            characterModel.SetState(CharacterModel.State.Idle);
        }

        public void Jump()
        {
            if (isJumping || !isGrounded) { return; }

            isJumping = true;
            
            var force = Vector3.up * gamePlayerInfo.jumpForce;
            rigidbody.AddForce(force, jumpForceMode);

            characterModel.SetState(CharacterModel.State.Jumping);
        }

        public void EnableMovement(bool enabled)
        {
            rigidbody.isKinematic = !enabled;
            constantForce.enabled = enabled;
        }

        void UpdateIsGrounded()
        {
            var wasGrounded = isGrounded;
            
            
            isGrounded = Physics.Raycast(transform.localPosition - (Vector3.down * 0.2f), Vector3.down, 0.25f, groundLayerMaskBytes);

            var isGroundedChange = wasGrounded != isGrounded;

            if (isGroundedChange)
            {
                if (isGrounded)
                {
                    isJumping = false;
                    characterModel.SetState(isRunning ? CharacterModel.State.Running : CharacterModel.State.Idle);
                }
                else
                {
                    characterModel.SetState(isJumping ? CharacterModel.State.Jumping : CharacterModel.State.Idle);
                }             
            }
        }
    }
}