using System;

namespace Game.Gameplay
{
    [Serializable]
    public class GamePlayerInfo
    {
        public float runSpeed;
        public float jumpForce;
    }
}