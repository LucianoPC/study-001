using UnityEngine;

namespace Game.Gameplay
{
    public class CharacterModel : MonoBehaviour
    {
        public enum State
        {
            Idle,
            Running,
            Jumping,
        }
        
        static readonly int Running = Animator.StringToHash("Running");
        static readonly int Jumping = Animator.StringToHash("Jumping");
        
        [SerializeField] Animator animator;

        public void SetState(State state)
        {
            switch (state)
            {
                case State.Idle:
                    animator.SetBool(Running, false);
                    animator.SetBool(Jumping, false);
                    break;
                case State.Running:
                    animator.SetBool(Running, true);
                    animator.SetBool(Jumping, false);
                    break;
                case State.Jumping:
                    animator.SetBool(Running, false);
                    animator.SetBool(Jumping, true);
                    break;
            }
        }
    }
}
