using System;
using System.Collections;
using Game.Helpers;
using Game.Models;
using Game.Models.Boxes;
using Game.Services;
using Game.UI.Views;
using UGameArch.Core;
using UGameArch.Presenters;
using UGameArch.Providers;
using UGameArch.UI;
using UnityEngine;
using CharacterInfo = Game.Models.CharacterInfo;

namespace Game.Presenters
{
    public class StatusPresenter
    {
        readonly PlayerInfoBox playerInfoBox;
        readonly CharacterInfoBox characterInfoBox;
        readonly CharacterUpgradeInfoBox characterUpgradeInfoBox;
        readonly StatusView statusView;
        readonly DialogPopupPresenter dialogPopup;
        readonly LoadingPopup loadingPopup;
        readonly CharacterService characterService;
        readonly SpriteProvider spriteProvider;
        readonly ICoroutineHelper coroutineHelper;

        Coroutine updateSpeedUpCoroutine;

        public StatusPresenter(
            PlayerInfoBox playerInfoBox,
            CharacterInfoBox characterInfoBox,
            CharacterUpgradeInfoBox characterUpgradeInfoBox,
            StatusView statusView,
            DialogPopup dialogPopup,
            LoadingPopup loadingPopup,
            CharacterService characterService,
            SpriteProvider spriteProvider,
            ICoroutineHelper coroutineHelper)
        {
            this.playerInfoBox = playerInfoBox;
            this.characterInfoBox = characterInfoBox;
            this.characterUpgradeInfoBox = characterUpgradeInfoBox;
            this.statusView = statusView;
            this.dialogPopup = new DialogPopupPresenter(dialogPopup, coroutineHelper);
            this.loadingPopup = loadingPopup;
            this.characterService = characterService;
            this.spriteProvider = spriteProvider;
            this.coroutineHelper = coroutineHelper;
        }

        public void Initialize()
        {
            statusView.LevelUpPressed += TryLevelUp;
            statusView.SpeedUpPressed += TrySpeedUp;
            
            SetUpdateViewCallbacksEnabled(true);
            UpdateStatusView();
        }

        public void Dispose()
        {
            statusView.LevelUpPressed -= TryLevelUp;
            statusView.SpeedUpPressed -= TrySpeedUp;
            
            SetUpdateViewCallbacksEnabled(false);
        }

        void TryLevelUp()
        {
            var characterId = playerInfoBox.Model.selectedCharacterId;
            
            SetUpdateViewCallbacksEnabled(false);
            
            loadingPopup.Show();
            characterService.LevelUp(characterId,
                onSuccess: () =>
                {
                    loadingPopup.Hide();
                    UpdateStatusView();
                    SetUpdateViewCallbacksEnabled(true);
                },
                onError: error =>
                {
                    loadingPopup.Hide();
                    SetUpdateViewCallbacksEnabled(true);
                    dialogPopup.ShowError(error, onConfirm: dialogPopup.Hide);
                });
        }

        void TrySpeedUp()
        {
            var characterId = playerInfoBox.Model.selectedCharacterId;
            
            SetUpdateViewCallbacksEnabled(false);
            
            loadingPopup.Show();
            characterService.SpeedUp(characterId,
                onSuccess: () =>
                {
                    loadingPopup.Hide();
                    UpdateStatusView();
                    SetUpdateViewCallbacksEnabled(true);
                },
                onError: error =>
                {
                    loadingPopup.Hide();
                    SetUpdateViewCallbacksEnabled(true);
                    dialogPopup.ShowError(error, onConfirm: dialogPopup.Hide);
                });
        }

        void UpdateStatusView()
        {
            var characterId = playerInfoBox.Model.selectedCharacterId;
            var characterInfo = characterInfoBox.GetCharacterInfo(characterId);
            
            var upgradeIndex = characterInfo.level - 1;
            var upgrades = characterUpgradeInfoBox.GetCharacterUpgradeInfo(characterId).upgradeItems;
            var hasNextUpgrade = upgradeIndex < upgrades.Count - 1;
            
            var currentUpgrade = upgrades[upgradeIndex];
            var nextUpgrade = hasNextUpgrade ? upgrades[upgradeIndex + 1] : null;
            
            statusView.SetLevelText($"Level {characterInfo.level:0}");
            statusView.SetSpeedText($"{currentUpgrade.speed:F2}");
            statusView.SetNextSpeedText(hasNextUpgrade ? $"+{(nextUpgrade.speed - currentUpgrade.speed):F2}" : string.Empty);

            if (hasNextUpgrade)
            {
                statusView.SetState(!characterInfo.isUpgrading ? StatusView.State.LevelUp : StatusView.State.SpeedUp);
                statusView.SetLevelUpCostText($"{nextUpgrade.levelUpCost:N0}");
                statusView.SetSpeedUpCostText($"{nextUpgrade.speedUpCost:N0}");
                statusView.SetLevelUpCostIconView(spriteProvider.GetSprite(nextUpgrade.levelUpCurrencyCode));
                statusView.SetSpeedUpCostIconView(spriteProvider.GetSprite(nextUpgrade.speedUpCurrencyCode));
                statusView.SetSpeedUpCostIconViewVisible(true);
                
                StartUpdateSpeedUpCoroutine(characterInfo, nextUpgrade);
            }
            else
            {
                statusView.SetState(StatusView.State.NoLevelUp);
            }
        }

        void StartUpdateSpeedUpCoroutine(CharacterInfo characterInfo, CharacterUpgradeItemInfo nextUpgrade)
        {
            if (!characterInfo.isUpgrading) { return; }
            if (updateSpeedUpCoroutine != null) { coroutineHelper.StopCoroutine(updateSpeedUpCoroutine); }

            coroutineHelper.StartCoroutine(UpdateSpeedUp(characterInfo, nextUpgrade));
        }

        IEnumerator UpdateSpeedUp(CharacterInfo characterInfo, CharacterUpgradeItemInfo nextUpgrade)
        {
            do
            {
                var timeNow = DateTime.UtcNow;
                var upgradedAt = DateTimeHelper.FromUnix(characterInfo.unixUpgradedAt);
                var hasTimeFinished = timeNow >= upgradedAt;

                if (hasTimeFinished)
                {
                    statusView.SetSpeedUpTimeText(string.Empty);
                    statusView.SetSpeedUpCostIconViewVisible(false);
                    statusView.SetSpeedUpCostText("Upgrade Now");
                    yield break;
                }

                var deltaTime = upgradedAt - timeNow;
                var deltaTimeText = $"{deltaTime.Hours:D2}:{deltaTime.Minutes:D2}:{deltaTime.Seconds:D2}";
                statusView.SetSpeedUpTimeText(deltaTimeText);
                
                yield return null;

            } while (true);
        }

        void SetUpdateViewCallbacksEnabled(bool isEnabled)
        {
            playerInfoBox.Updated -= UpdateStatusView;
            characterInfoBox.Updated -= UpdateStatusView;
            characterUpgradeInfoBox.Updated -= UpdateStatusView;

            if (isEnabled)
            {
                playerInfoBox.Updated += UpdateStatusView;
                characterInfoBox.Updated += UpdateStatusView;
                characterUpgradeInfoBox.Updated += UpdateStatusView;
            }
        }
    }
}