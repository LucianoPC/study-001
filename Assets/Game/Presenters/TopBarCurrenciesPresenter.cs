using Game.Constants;
using Game.Models;
using Game.Models.Boxes;
using UGameArch.Core;
using UGameArch.Helpers;
using UGameArch.Providers;
using UnityEngine;

namespace Game.UI.Views.TopBar
{
    public class TopBarCurrenciesPresenter
    {
        readonly WalletInfoBox walletInfoBox;
        readonly TopBarCurrenciesView topBarView;
        readonly SpriteProvider spriteProvider;
        readonly ICoroutineHelper coroutineHelper;
        
        readonly CanvasGroup canvasGroup;
        
        Coroutine showCoroutine;
        
        public TopBarCurrenciesPresenter(WalletInfoBox walletInfoBox,
            TopBarCurrenciesView topBarView,
            SpriteProvider spriteProvider,
            ICoroutineHelper coroutineHelper)
        {
            this.walletInfoBox = walletInfoBox;
            this.topBarView = topBarView;
            this.spriteProvider = spriteProvider;
            this.coroutineHelper = coroutineHelper;

            canvasGroup = CanvasGroupHelper.AddCanvasGroup(topBarView.gameObject);
        }

        public void Initialize()
        {
            walletInfoBox.Updated += UpdateCurrencyViews;
            
            UpdateCurrencyViews();
        }

        public void Dispose()
        {
            walletInfoBox.Updated -= UpdateCurrencyViews;
        }

        public void Show()
        {
            topBarView.gameObject.SetActive(true);
            showCoroutine = coroutineHelper.StartCoroutine(CanvasGroupHelper.CoroutineFadeIn(canvasGroup));
        }

        public void Hide()
        {
            coroutineHelper.StopCoroutine(showCoroutine);
            topBarView.gameObject.SetActive(false);
        }
        
        void UpdateCurrencyViews()
        {
            UpdateSoftCurrencyView();
            UpdateHardCurrencyView();
        }

        void UpdateSoftCurrencyView()
        {
            topBarView.SoftCurrencyView.SetText(walletInfoBox.Model.GetCurrency(Code.Currency.Soft).ToString("N0"));
            topBarView.SoftCurrencyView.SetIcon(spriteProvider.GetSprite(Code.Currency.Soft));
        }

        void UpdateHardCurrencyView()
        {
            topBarView.HardCurrencyView.SetText(walletInfoBox.Model.GetCurrency(Code.Currency.Hard).ToString("N0"));
            topBarView.HardCurrencyView.SetIcon(spriteProvider.GetSprite(Code.Currency.Hard));
        }
    }
}