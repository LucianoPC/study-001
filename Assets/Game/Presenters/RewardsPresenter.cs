using Game.Models;
using Game.UI.Views;
using UGameArch.Providers;

namespace Game.Presenters
{
    public class RewardsPresenter
    {
        readonly RewardsView rewardsView;
        readonly SpriteProvider spriteProvider;
        readonly RewardInfo[] rewardInfos;

        public RewardsPresenter(
            RewardsView rewardsView,
            SpriteProvider spriteProvider,
            RewardInfo[] rewardInfos)
        {
            this.rewardsView = rewardsView;
            this.spriteProvider = spriteProvider;
            this.rewardInfos = rewardInfos;
        }

        public void Initialize()
        {
            UpdateRewardsView();
        }

        public void Dispose()
        {
        }

        void UpdateRewardsView()
        {
            var rewardItems = new RewardsView.RewardItem[rewardInfos.Length];

            for (var i = 0; i < rewardItems.Length; i++)
            {
                rewardItems[i] = new RewardsView.RewardItem
                {
                    Icon = spriteProvider.GetSprite(rewardInfos[i].code),
                    Amount = rewardInfos[i].amount.ToString("N0")
                };
            }
            
            rewardsView.SetRewardItems(rewardItems);
        }
    }
}