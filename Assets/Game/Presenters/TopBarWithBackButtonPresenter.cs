using System;
using Game.Constants;
using Game.Models;
using Game.Models.Boxes;
using Game.UI.Views.TopBar;
using UGameArch.Providers;

namespace Game.Presenters
{
    public class TopBarWithBackButtonPresenter
    {
        readonly WalletInfoBox walletInfoBox;
        readonly TopBarWithBackButtonView topBarView;
        readonly SpriteProvider spriteProvider;
        
        public event Action BackButtonPressed;

        public TopBarWithBackButtonPresenter(string title,
            WalletInfoBox walletInfoBox,
            TopBarWithBackButtonView topBarView,
            SpriteProvider spriteProvider)
        {
            this.walletInfoBox = walletInfoBox;
            this.topBarView = topBarView;
            this.spriteProvider = spriteProvider;
            
            topBarView.SetTitle(title);
        }

        public void Initialize()
        {
            walletInfoBox.Updated += UpdateCurrencyViews;
            topBarView.BackButtonPressed += OnBackButtonPressed;
            
            UpdateCurrencyViews();
        }

        public void Dispose()
        {
            walletInfoBox.Updated -= UpdateCurrencyViews;
            topBarView.BackButtonPressed -= OnBackButtonPressed;
        }
        
        void OnBackButtonPressed()
        {
            BackButtonPressed?.Invoke();
        }

        void UpdateCurrencyViews()
        {
            UpdateSoftCurrencyView();
            UpdateHardCurrencyView();
        }

        void UpdateSoftCurrencyView()
        {
            topBarView.SoftCurrencyView.SetText(walletInfoBox.Model.GetCurrency(Code.Currency.Soft).ToString("N0"));
            topBarView.SoftCurrencyView.SetIcon(spriteProvider.GetSprite(Code.Currency.Soft));
        }

        void UpdateHardCurrencyView()
        {
            topBarView.HardCurrencyView.SetText(walletInfoBox.Model.GetCurrency(Code.Currency.Hard).ToString("N0"));
            topBarView.HardCurrencyView.SetIcon(spriteProvider.GetSprite(Code.Currency.Hard));
        }
    }
}