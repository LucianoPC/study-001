using Game.Models.Boxes;
using Game.UI.Views;

namespace Game.Presenters
{
    public class LeaderboardPresenter
    {
        readonly PlayerInfoBox playerInfoBox;
        readonly LeaderboardInfoBox leaderboardInfoBox;
        readonly LeaderboardView leaderboardView;

        public LeaderboardPresenter(
            PlayerInfoBox playerInfoBox,
            LeaderboardInfoBox leaderboardInfoBox,
            LeaderboardView leaderboardView)
        {
            this.playerInfoBox = playerInfoBox;
            this.leaderboardInfoBox = leaderboardInfoBox;
            this.leaderboardView = leaderboardView;
        }

        public void Initialize()
        {
            leaderboardInfoBox.Updated += UpdateLeaderboardView;
            
            UpdateLeaderboardView();
        }

        public void Dispose()
        {
            leaderboardInfoBox.Updated -= UpdateLeaderboardView;
        }

        void UpdateLeaderboardView()
        {
            var leaderboardItemInfos = leaderboardInfoBox.Model.items;
            var leaderboardItems = new LeaderboardView.LeaderboardItem[leaderboardItemInfos.Length];

            for (var i = 0; i < leaderboardItemInfos.Length; i++)
            {
                var itemInfo = leaderboardItemInfos[i];

                leaderboardItems[i] = new LeaderboardView.LeaderboardItem
                {
                    Position = $"{i+1}.",
                    Name = itemInfo.isCurrentPlayer ? playerInfoBox.Model.name : itemInfo.playerName,
                    BestTime = itemInfo.bestTimeSeconds.ToString("N3"),
                    IsHighlighted = itemInfo.isCurrentPlayer,
                };
            }
            
            leaderboardView.SetLeaderboardItems(leaderboardItems);
        }
    }
}