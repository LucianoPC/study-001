using System;
using Game.Constants;
using Game.Models.Boxes;
using Game.Services;
using Game.UI.Views.TopBar;
using UGameArch.Core;
using UGameArch.Presenters;
using UGameArch.Providers;
using UGameArch.UI;

namespace Game.Presenters
{
    public class TopBarPresenter
    {
        readonly PlayerInfoBox playerInfoBox;
        readonly WalletInfoBox walletInfoBox;
        readonly TopBarView topBarView;
        readonly DialogPopupPresenter dialogPopup;
        readonly LoadingPopup loadingPopup;
        readonly PlayerService playerService;
        readonly SpriteProvider spriteProvider;

        readonly TextInputPopupPresenter textInputPopupPresenter;

        public TopBarPresenter(PlayerInfoBox playerInfoBox,
            WalletInfoBox walletInfoBox,
            TopBarView topBarView,
            DialogPopup dialogPopup,
            LoadingPopup loadingPopup,
            TextInputPopup textInputPopup,
            PlayerService playerService,
            SpriteProvider spriteProvider,
            ICoroutineHelper coroutineHelper)
        {
            this.playerInfoBox = playerInfoBox;
            this.walletInfoBox = walletInfoBox;
            this.topBarView = topBarView;
            this.dialogPopup = new DialogPopupPresenter(dialogPopup, coroutineHelper);
            this.loadingPopup = loadingPopup;
            this.playerService = playerService;
            this.spriteProvider = spriteProvider;

            textInputPopupPresenter = new TextInputPopupPresenter(textInputPopup, coroutineHelper);
        }

        public void Initialize()
        {
            playerInfoBox.Updated += UpdatePlayerNameView;
            walletInfoBox.Updated += UpdateCurrencyViews;
            topBarView.PlayerNameView.Clicked += OnPlayerNameViewClicked;
            
            UpdateCurrencyViews();
            UpdatePlayerNameView();
        }

        public void Dispose()
        {
            playerInfoBox.Updated -= UpdatePlayerNameView;
            walletInfoBox.Updated -= UpdateCurrencyViews;
            topBarView.PlayerNameView.Clicked -= OnPlayerNameViewClicked;
        }

        void OnPlayerNameViewClicked()
        {
            textInputPopupPresenter.Show(
                title: "Choose your name",
                inputText: playerInfoBox.Model.name,
                confirmButtonText: "Confirm",
                cancelButtonText: "Cancel",
                canBackgroundCancel: true,
                onConfirm: result =>
                {
                    loadingPopup.Show();
                    playerService.SetName(result.InputText,
                        onSuccess: () =>
                        {
                            loadingPopup.Hide();
                            textInputPopupPresenter.Hide();
                        },
                        onError: error =>
                        {
                            loadingPopup.Hide();
                            dialogPopup.Show(
                                title: "Error",
                                message: $"{error.Message}",
                                buttonText: "Ok",
                                canBackgroundConfirm: false,
                                buttonType: DialogPopup.ButtonType.Confirm,
                                onConfirm: dialogPopup.Hide);
                        });
                },
                onCancel: () =>
                {
                    textInputPopupPresenter.Hide();
                });
        }

        void UpdateCurrencyViews()
        {
            UpdateSoftCurrencyView();
            UpdateHardCurrencyView();
        }

        void UpdateSoftCurrencyView()
        {
            topBarView.SoftCurrencyView.SetText(walletInfoBox.Model.GetCurrency(Code.Currency.Soft).ToString("N0"));
            topBarView.SoftCurrencyView.SetIcon(spriteProvider.GetSprite(Code.Currency.Soft));
        }

        void UpdateHardCurrencyView()
        {
            topBarView.HardCurrencyView.SetText(walletInfoBox.Model.GetCurrency(Code.Currency.Hard).ToString("N0"));
            topBarView.HardCurrencyView.SetIcon(spriteProvider.GetSprite(Code.Currency.Hard));
        }

        void UpdatePlayerNameView()
        {
            topBarView.PlayerNameView.SetText(playerInfoBox.Model.name);
        }
    }
}