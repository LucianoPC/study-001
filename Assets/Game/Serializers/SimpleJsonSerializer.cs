using System.Collections.Generic;
using Game.Models;
using SimpleJson;
using UGameArch.Core;

namespace Game.Serializers
{
    public class SimpleJsonSerializer : ISerializer
    {
        public string ToJson(object obj)
        {
            var data = string.Empty;

            if (obj is string objString)
            {
                data = objString;
            }
            else if (obj != null)
            {
                data = SimpleJson.SimpleJson.SerializeObject(obj);
            }

            return data;
        }

        public T FromJson<T>(string json)
        {
            if (typeof(T) == typeof(string)) { return (T)(object)json; }

            return SimpleJson.SimpleJson.DeserializeObject<T>(json);
        }
    }
}