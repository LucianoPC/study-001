using System;
using System.Linq;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace _Game.Editor
{
    public class Builder
    {
        const string appName = "jump-man";

        static BuildTargetGroup[] targets =
        {
            BuildTargetGroup.iOS,
            BuildTargetGroup.Android,
            BuildTargetGroup.Standalone,
        };

        static string[] ClientScenes => EditorBuildSettings.scenes.Select(s => s.path).ToArray();

        [MenuItem("Tools/Game/Builder/Build LOCAL Client MacOS")]
        public static void BuildLocalClientMacOs()
        {
            var buildTarget = BuildTarget.StandaloneOSX;
            var buildFolder = "StandaloneOSX";
            var applicationName = $"{appName}.app";

            var flags = new[]
            {
                "NO_CACHE",
            };

            BuildClient(buildTarget, buildFolder, applicationName, ClientScenes, flags);
        }

        [MenuItem("Tools/Game/Builder/Build PROD Client MacOS")]
        public static void BuildProdClientMacOs()
        {
            var buildTarget = BuildTarget.StandaloneOSX;
            var buildFolder = "StandaloneOSX";
            var applicationName = $"{appName}.app";

            var flags = new[]
            {
                "NO_CACHE",
                "PROD_CLIENT",
            };

            BuildClient(buildTarget, buildFolder, applicationName, ClientScenes, flags);
        }

        [MenuItem("Tools/Game/Builder/Build PROD Client Android")]
        public static void BuildClientAndroid()
        {
            var buildTarget = BuildTarget.Android;
            var buildFolder = "Android";
            var applicationName = $"{appName}.apk";

            var flags = new string[]
            {
                "PROD_CLIENT",
            };

            BuildClient(buildTarget, buildFolder, applicationName, ClientScenes, flags);
        }

        [MenuItem("Tools/Game/Builder/Build Server Linux")]
        public static void BuildServerLinux()
        {
            var buildTarget = BuildTarget.StandaloneLinux64;
            var buildFolder = "StandaloneLinuxServer";
            var applicationName = $"{appName}-game-server-linux";

            var scenes = new[]
            {
                "Assets/_Game/Scenes/Gameplay.unity",
            };

            BuildServer(buildTarget, buildFolder, applicationName, scenes);
        }


        public static void BuildClient(BuildTarget buildTarget, string applicationParentFolder, string fileName, string[] scenes, params string[] flags)
        {
            Build(buildTarget, BuildOptions.None, applicationParentFolder, fileName, scenes, flags);
        }

        public static void BuildServer(BuildTarget buildTarget, string applicationParentFolder, string fileName, string[] scenes)
        {
            var flags = new[] { "UNITY_SERVER" };

            Build(buildTarget, BuildOptions.EnableHeadlessMode, applicationParentFolder, fileName, scenes, flags);
        }

        static void Build(BuildTarget buildTarget, BuildOptions buildOptions, string applicationParentFolder,
            string fileName, string[] scenes, params string[] flags)
        {
            PrepareBuildFolders(applicationParentFolder);

            var defineSymbols = GetDefineSymbolsForGroup();

            foreach (var flag in flags) { AddFlag(flag); }

            var buildPlayerOptions = new BuildPlayerOptions();

            buildPlayerOptions.scenes = scenes;
            buildPlayerOptions.locationPathName = $"Build/{applicationParentFolder}/{fileName}";
            buildPlayerOptions.target = buildTarget;
            buildPlayerOptions.options = buildOptions;

            var report = BuildPipeline.BuildPlayer(buildPlayerOptions);

            if (report.summary.result == BuildResult.Succeeded)
            {
                Debug.LogFormat("Build succeeded: [path: {0}] [bytes: {1}]", buildPlayerOptions.locationPathName, report.summary.totalSize);
            }
            else if (report.summary.result == BuildResult.Failed)
            {
                Debug.Log("Build failed");
            }

            // foreach (var flag in flags) { RemoveFlag(flag); }

            SetDefineSymbolsForGroup(defineSymbols);

            AssetDatabase.SaveAssets();
        }

        static void PrepareBuildFolders(string applicationParentFolder)
        {
            bool existsBuildFolder = System.IO.Directory.Exists("Build");

            if (!existsBuildFolder) { System.IO.Directory.CreateDirectory("Build"); }

            bool existsApplicationParentFolder = System.IO.Directory.Exists(applicationParentFolder);

            if (!existsApplicationParentFolder) { System.IO.Directory.CreateDirectory(applicationParentFolder); }
        }

        static void AddFlag(string flagString)
        {
            foreach (var target in targets)
            {
                var definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(target);

                if (definesString.Contains(flagString))
                {
                    continue;
                }

                PlayerSettings.SetScriptingDefineSymbolsForGroup(target, flagString + ";" + definesString);
            }
        }

        static void RemoveFlag(string flagString)
        {
            foreach (var target in targets)
            {
                var definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(target);
                var defines = definesString.Replace(flagString, "");
                PlayerSettings.SetScriptingDefineSymbolsForGroup(target, defines);
            }
        }

        static string[] GetDefineSymbolsForGroup()
        {
            return targets.Select(PlayerSettings.GetScriptingDefineSymbolsForGroup).ToArray();
        }

        static void SetDefineSymbolsForGroup(string[] defines)
        {
            var minSize = Math.Min(targets.Length, defines.Length);

            for (var i = 0; i < minSize; i++)
            {

                Debug.LogFormat("[PRESTES] DEFINE SYMBOLS :: [target: {0}] [symbols: {1}]", targets[i], defines[i]);
                PlayerSettings.SetScriptingDefineSymbolsForGroup(targets[i], defines[i]);
            }
        }
    }
}
