namespace Game.Constants
{
    public static class Code
    {
        public const string Fail = "fail";
        
        public static class Currency
        {
            public const string Soft = "softCurrency";
            public const string Hard = "hardCurrency";
        }

        public static class Error
        {
            public const string NotEnoughCurrency = "Error-001";
        }
    }
}