namespace Game.Constants
{
    public static class SceneName
    {
        public const string Gameplay = "Gameplay";
        public const string MainMenu = "MainMenu";
    }
}