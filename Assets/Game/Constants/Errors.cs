namespace Game.Constants
{
    public static class Errors
    {
        public static class Code
        {
            public const string NotEnoughCurrency = "Error-001";
            public const string NotHasNextUpgrade = "Error-002";
            public const string WaitUpgrade = "Error-003";
            public const string NotUpgrading = "Error-003";
        }
        
        public static class Message
        {
            public const string NotEnoughCurrency = "Not enough currency";
            public const string NotHasNextUpgrade = "Not has next upgrade";
            public const string WaitUpgrade = "Wait upgrade finish!";
            public const string NotUpgrading = "You need upgrade before speed up";
        }
    }
}